//
//  MainViewController.h
//  iOS
//
//  Created by 李坤 on 16/11/22.
//
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *myLabel;

@property (strong, nonatomic) IBOutlet UIButton *redBtn;

@property (strong, nonatomic) IBOutlet UIButton *blueBtn;

@property (strong, nonatomic) IBOutlet UIButton *greenBtn;

@end
