//
//  MainViewController.m
//  iOS
//
//  Created by 李坤 on 16/11/22.
//
//

#import "MainViewController.h"

@interface MainViewController ()


@end

@implementation MainViewController
- (IBAction)xxx:(id)sender forEvent:(UIEvent *)event {
}

- (IBAction)redBtnClick
{
    NSLog(@"%s",__func__);
    //设置文字颜色
    self.myLabel.textColor = [UIColor redColor];
    //设置背景颜色
    self.myLabel.backgroundColor = [UIColor purpleColor];
}

- (IBAction)blueBtnClick
{
    NSLog(@"%s",__func__);
    self.myLabel.textColor = [UIColor blueColor];
    
}

- (IBAction)greenBtnClick
{
    NSLog(@"%s",__func__);
    self.myLabel.textColor = [UIColor greenColor];
    NSLog(@"%@",self.view.subviews);
    NSLog(@"%lu",self.view.subviews.count);
}

@end
