//
//  ViewController.m
//  3.1、UILabel的使用
//
//  Created by 李坤 on 16/11/29.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self makeUILabel];
}

/**
 *  创建一个UILabel
 */
- (void)makeUILabel{
    //1.创建UILable对象
    UILabel *label = [[UILabel alloc] init];
    
    //2.设置frame（位置和尺寸）
    label.frame = CGRectMake(100, 100, 175, 200);
    
    //3.设置背景
    label.backgroundColor = [UIColor yellowColor];
    
    //4.设置显示的文字
    label.text = @"hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world";
    
    //5.设置文字的颜色
    label.textColor = [UIColor blueColor];
    
    //6.设置labe的行数
    label.numberOfLines = 0;
    
    //7.设置字体大小
    label.font = [UIFont systemFontOfSize:18];
    //设置字体粗体大小
    label.font = [UIFont boldSystemFontOfSize:18];
    //设置斜体
    label.font = [UIFont italicSystemFontOfSize:18];
    
    //8.设置文字居中
    label.textAlignment = NSTextAlignmentCenter;
    
    //9.省略号的位置或者剪切，字符串或者单词的完整性
    label.lineBreakMode = NSLineBreakByCharWrapping;
    
    //10.设置阴影
    //10.1.设置阴影颜色
    label.shadowColor = [UIColor redColor];
    //10.2.设置阴影偏移量
    label.shadowOffset = CGSizeMake(.5, .5);
    
    //11.将对象添加到控制器的view中
    [self.view addSubview:label];
}

@end
