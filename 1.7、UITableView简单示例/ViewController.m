//
//  ViewController.m
//  1.7、UITableView简单示例
//
//  Created by 李坤 on 16/12/1.
//
//

#import "ViewController.h"

@interface ViewController ()<UITableViewDataSource>

@property(nonatomic,weak)UITableView *tableView;

//@property(nonatomic,strong)

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createTableView];
}

/**
 *  告诉tableview一共有多少组
 *
 *  @param tableView tableview对象
 *
 *  @return 返回一共有多少组
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSLog(@"%s",__func__);
    return 2;
}

/**
 *  第section组有多少行
 *
 *  @param tableView tableview对象
 *  @param section   组下标
 *
 *  @return 当前组有多少行
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"%s",__func__);
    if(section == 0){
        //第0组有多少行
        return 2;
    }else{
        //第1组有多少行
        return 3;
    }
}

/**
 *  告知每一行显示什么内容
 *
 *  @param tableView tableview对象
 *  @param indexPath 行下标
 *
 *  @return 每行内容
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%s",__func__);
    //1.创建cell
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    //2.设置数据
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            cell.textLabel.text = @"奥迪";
        }else if(indexPath.row == 1){
            cell.textLabel.text = @"宝马";
        }
    }else if(indexPath.section == 1){
        if(indexPath.row == 0){
            cell.textLabel.text = @"本田";
        }else if(indexPath.row == 1){
            cell.textLabel.text = @"丰田";
        }else if(indexPath.row == 2){
            cell.textLabel.text = @"马自达";
        }
    }
    return cell;
}

/**
 *  第section组头部显示什么标题
 *
 *  @param tableView tableview对象
 *  @param section   组下标
 *
 *  @return 第section组头部内容
 */
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section==0){
        return @"德系品牌";
    }else{
        return @"日韩品牌";
    }
}

/**
 *  第section组底部显示什么标题
 *
 *  @param tableView tableview对象
 *  @param section   组下标
 *
 *  @return 第section组底部内容
 */
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    if(section == 0){
        return @"高端大气上档次";
    }else{
        return @"还不错";
    }
}

/**
 *  创建tableview
 */
- (void)createTableView{
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.dataSource = self;
    [self.view addSubview:tableView];
}

@end
