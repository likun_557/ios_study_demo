//
//  HeroModel.m
//  iOS
//
//  Created by 李坤 on 16/12/6.
//
//

#import "HeroModel.h"

@implementation HeroModel

- (instancetype)initWithName:(NSString *)name andIcon:(NSString *)icon{
    if(self = [super init]){
        self.name = name;
        self.icon = icon;
    }
    return self;
}

+ (instancetype)heroWithName:(NSString *)name andIcon:(NSString *)icon{
    return [[self alloc] initWithName:name andIcon:icon];
}

+ (instancetype)heroWithDictionary:(NSDictionary *)dict{
    id obj = [[self alloc] init];
    [obj setValuesForKeysWithDictionary:dict];
    NSLog(@"%@",obj);
    return obj;
}

+ (NSMutableArray<HeroModel *> *)herosWithArrayOfDict:(NSArray<NSDictionary *> *)dicts{
    NSMutableArray<HeroModel *> *result = [[NSMutableArray alloc] init];
    NSUInteger len = dicts.count;
    for(int i = 0; i < len; i++){
        [result addObject:[self heroWithDictionary:dicts[i]]];
    }
    return result;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

- (NSString *)description{
    return [NSString stringWithFormat:@"name = %@,icon = %@",self.name,self.icon];
}

@end
