//
//  LFViewController.m
//  iOS
//
//  Created by 李坤 on 16/12/6.
//
//

#import "LFViewController.h"
#import "HeroModel.h"

@interface LFViewController ()

@property(nonatomic,strong)NSArray<HeroModel *> *appList;

@end

@implementation LFViewController

//使用懒加载获取数据
- (NSArray *)appList{
    if(!_appList) {
        //从mainbundle加载数据
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *path = [bundle pathForResource:@"app" ofType:@"plist"];
        NSLog(@"%@",path);
        NSArray *array = [NSArray arrayWithContentsOfFile:path];
        _appList = [HeroModel herosWithArrayOfDict:array];
        NSLog(@"%@",_appList);
//        NSLog(@"%@",[HeroModel herosWithArrayOfDict:_appList]);
        
    }
    return _appList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //总共有3列
    int totalCol = 3;
    CGFloat viewW = 80;
    CGFloat viewH = 90;
    NSInteger marginX = (self.view.bounds.size.width - totalCol * viewW) / (totalCol+1);
    CGFloat marginY = 10;
    CGFloat startY = 20;
    for(int i = 0; i < self.appList.count; i++){
        
        int row = i / totalCol;
        int col = i % totalCol;
        CGFloat x = marginX + (viewW + marginX) * col;
        CGFloat y = startY + marginY + (viewH + marginY) * row;
        
        HeroModel *heroModel = self.appList[i];
        //创建view
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(x, y, viewW, viewH)];
        view.backgroundColor = [UIColor redColor];
        [self.view addSubview:view];
        
        //创建一个图片放在view中
        UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:heroModel.icon]];
        image.clipsToBounds = YES;
        image.frame = CGRectMake(1, 1 , view.bounds.size.width-2, viewH-20);
        image.contentMode = UIViewContentModeScaleAspectFill;
        [view addSubview:image];
        
        //创建英雄名称
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(1, image.bounds.size.height+1, view.bounds.size.width-2, 20-2)];
        label.text = heroModel.name;
        label.textColor = [UIColor blueColor];
        label.font = [UIFont boldSystemFontOfSize:20];
        label.backgroundColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        [view addSubview:label];        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
