//
//  HeroModel.h
//  iOS
//
//  Created by 李坤 on 16/12/6.
//
//

#import <Foundation/Foundation.h>

@interface HeroModel : NSObject
/**
 *  名称
 */
@property(nonatomic,copy)NSString *name;
/**
 *  图片
 */
@property(nonatomic,copy)NSString *icon;

/**
 *  初始化对象
 *
 *  @param name 名称
 *  @param icon 图片
 *
 *  @return 返回初始化的对象
 */
- (instancetype)initWithName:(NSString *)name andIcon:(NSString *)icon;

/**
 *  初始化对象
 *
 *  @param name 名称
 *  @param icon 图片
 *
 *  @return 返回初始化的对象
 */
+ (instancetype)heroWithName:(NSString *)name andIcon:(NSString *)icon;

/**
 *  根据字典创建对象
 *
 *  @param dict 字典
 *
 *  @return 返回创建的对象
 */
+ (instancetype)heroWithDictionary:(NSDictionary *)dict;

/**
 *  根据字典数组创建对象列表
 *
 *  @param dicts 字典数组
 *
 *  @return 返回创建的对象列表
 */
+ (NSMutableArray<HeroModel *> *)herosWithArrayOfDict:(NSArray<NSDictionary *> *)dicts;
@end
