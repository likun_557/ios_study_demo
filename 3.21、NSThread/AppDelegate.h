//
//  AppDelegate.h
//  3.21、NSThread
//
//  Created by 李坤 on 16/12/13.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

