//
//  ViewController.m
//  3.21、NSThread
//
//  Created by 李坤 on 16/12/13.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [NSThread currentThread].name = @"我是主线程";
    
    [self m7];
    
    [self th2Run:nil];
    NSLog(@"--------");
    

}

//1、创建线程方式1
- (void)m1{
    
    /*
     - (instancetype)initWithTarget:(id)target selector:(SEL)selector object:(nullable id)argument
     target:线程执行方法所属的目标对象
     selector：线程执行的目标对象中的方法
     argument：执行方法传入的参数
     */
    NSThread *th1 = [[NSThread alloc] initWithTarget:self selector:@selector(th1Run:) object:@"哈哈"];
    th1.name = @"thread1";//设置线程的名称
    //2、启动线程
    [th1 start];
}

//创建线程方式2
- (void)m2{
    
    NSThread *th2 = [[NSThread alloc] initWithBlock:^{
        NSLog(@"小星星");
        //判断是否是主线程
        NSLog([NSThread isMainThread]?@"是主线程":@"不是主线程");
    }];
    [th2 start];
}

//创建线程方式3,创建并立即执行
- (void)m3{
    [NSThread detachNewThreadWithBlock:^{
        [NSThread currentThread].name = @"我是线程3，创建完成自动启动";
        int result = 0;
        for (int i = 0; i < 100; i++) {
            [NSThread sleepForTimeInterval:0.1];
            result+=i;
            NSLog(@"%i",i);
        }
        NSLog(@"%@,%i",[NSThread currentThread],result);
    }];
}

//创建线程方式4，创建并立即执行
- (void)m4{
    [NSThread detachNewThreadSelector:@selector(th1Run:) toTarget:self withObject:@"我是线程4，创建完成自动启动"];
}

//创建线程方式5
- (void)m5{
    [self performSelectorInBackground:@selector(th1Run:) withObject:@"我是线程5，创建之后立即执行"];
}

//在子线程中调用主线程方法
- (void)m6{
    [[[NSThread alloc] initWithBlock:^{
        [NSThread currentThread].name = @"我是子线程6";
        
        [self performSelectorOnMainThread:@selector(th2Run:) withObject:@"在子线程中执行主线程的方法，并阻塞子线程" waitUntilDone:NO];
        
        int result = 0;
        for (int i = 0; i < 20; i++) {
            [NSThread sleepForTimeInterval:0.1];
            result+=i;
            NSLog(@"%s,%i",__func__,i);
        }
        NSLog(@"%@,%i",[NSThread currentThread],result);
    }] start];
}

- (void)m7{
    
    [NSThread detachNewThreadWithBlock:^{
        NSThread *th = [[NSThread alloc] init];
        th.name = @"我是线程7";
        NSLog(@"----%@",th.name);
        [self performSelector:@selector(th2Run:) onThread:th withObject:th.name waitUntilDone:NO];
        
        NSLog(@"----%@",th.name);
    }];
    NSLog(@"%s",__func__);
    
}

- (void)th1Run:(NSObject *)obj{
    NSLog(@"%@",[NSThread currentThread]);
    NSLog(@"参数:%@",obj);
}


- (void)th2Run:(NSObject *)obj{
    int result = 0;
    for (int i = 0; i < 10; i++) {
        [NSThread sleepForTimeInterval:0.1];
        result+=i;
        NSLog(@"%s,%i,%@",__func__,i,obj);
        NSLog(@"%@",[NSThread currentThread]);
        NSLog(@"%@",[NSThread currentThread].isMainThread?@"是":@"否");
    }
    NSLog(@"%@,%i,%@",[NSThread currentThread].name,result,obj);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
