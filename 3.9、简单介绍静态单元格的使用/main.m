//
//  main.m
//  3.9、简单介绍静态单元格的使用
//
//  Created by 李坤 on 16/12/8.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
