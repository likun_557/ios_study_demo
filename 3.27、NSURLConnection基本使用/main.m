//
//  main.m
//  3.27、NSURLConnection基本使用
//
//  Created by 李坤 on 16/12/16.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
