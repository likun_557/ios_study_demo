//
//  BorrowModel.h
//  iOS
//
//  Created by 李坤 on 16/12/16.
//
//

#import <Foundation/Foundation.h>

@interface BorrowModel : NSObject
//标的id
@property(nonatomic,assign)long borrow_id;
//标题
@property(nonatomic,copy)NSString *title;
//金额
@property(nonatomic,copy)NSString *price;
//年化率
@property(nonatomic,assign)double apr;
//完成比例
@property(nonatomic,assign)double rate;
//状态
@property(nonatomic,assign)int status;
//开始时间
@property(nonatomic,assign)long starttime;
//结束时间
@property(nonatomic,assign)long end_time;
//期限
@property(nonatomic,assign)int limit;
//剩余时间
@property(nonatomic,copy)NSString *timestr;

+ (instancetype)borrowWithDict:(NSDictionary *)dict;
@end
