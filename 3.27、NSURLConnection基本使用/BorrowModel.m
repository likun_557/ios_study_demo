//
//  BorrowModel.m
//  iOS
//
//  Created by 李坤 on 16/12/16.
//
//

#import "BorrowModel.h"

@implementation BorrowModel

- (void)dealloc
{
//    [super dealloc];
    NSLog(@"%s,%@",__func__,self);
}

+ (instancetype)borrowWithDict:(NSDictionary *)dict{
    BorrowModel *bm = [[self alloc] init];
    [bm setValuesForKeysWithDictionary:dict];
    return bm;
}
/*
 //标的id
 @property(nonatomic,assign)long borrow_id;
 //标题
 @property(nonatomic,copy)NSString *title;
 //金额
 @property(nonatomic,assign)double price;
 //年化率
 @property(nonatomic,assign)double apr;
 //状态
 @property(nonatomic,assign)int status;
 //开始时间
 @property(nonatomic,assign)long start_time;
 //结束时间
 @property(nonatomic,assign)long end_time;
 //期限
 @property(nonatomic,assign)double limit;
 //剩余时间
 @property(nonatomic,copy)NSString *timestr;
 */
- (NSString *)description{
    return [NSString stringWithFormat:@"borrow_id = %lu,title = %@,price = %@,apr = %5.2f,rate = %5.2f,status = %i,starttime = %lu,end_time = %lu,limit = %i,timestr = %@",self.borrow_id,self.title,self.price,self.apr,self.rate,self.status,self.starttime,self.end_time,self.limit,self.timestr];
}

@end
