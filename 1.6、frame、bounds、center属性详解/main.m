//
//  main.m
//  1.6、frame、bounds、center属性详解
//
//  Created by 李坤 on 16/11/30.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
