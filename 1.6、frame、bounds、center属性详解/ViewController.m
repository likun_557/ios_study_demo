//
//  ViewController.m
//  1.6、frame、bounds、center属性详解
//
//  Created by 李坤 on 16/11/30.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self test1];
    
}

- (void)test1{

    self.view.backgroundColor = [UIColor yellowColor];
    
    UIView *view1 = [[UIView alloc] init];
    view1.frame = CGRectMake(100, 50, 100, 200);
    view1.backgroundColor = [UIColor blueColor];
    [self.view addSubview:view1];
    
    UIView *view2 = [[UIView alloc] init];
    view2.frame = CGRectMake(25, 50, 50, 100);
    view2.backgroundColor = [UIColor redColor];
    [view1 addSubview:view2];
    
    NSLog(@"view1.frame = %@",NSStringFromCGRect(view1.frame));
    NSLog(@"view1.bounds = %@",NSStringFromCGRect(view1.bounds));
    NSLog(@"view1.center = %@",NSStringFromCGPoint(view1.center));
    NSLog(@"-------------------------------");
    NSLog(@"view1.frame = %@",NSStringFromCGRect(view2.frame));
    NSLog(@"view1.bounds = %@",NSStringFromCGRect(view2.bounds));
    NSLog(@"view1.center = %@",NSStringFromCGPoint(view2.center));
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
