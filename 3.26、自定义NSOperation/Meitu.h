//
//  Meitu.h
//  iOS
//
//  Created by 李坤 on 16/12/16.
//
//

#import <Foundation/Foundation.h>

@interface Meitu : NSObject
@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *imgUrl;

- (instancetype)initWithTitle:(NSString *)title andImgUrl:(NSString *)imgUrl;

+ (instancetype)meituWithTitle:(NSString *)title andImgUrl:(NSString *)imgUrl;
@end
