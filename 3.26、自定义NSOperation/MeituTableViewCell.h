//
//  MeituTableViewCell.h
//  iOS
//
//  Created by 李坤 on 16/12/16.
//
//

#import <UIKit/UIKit.h>
@class Meitu;
@class MeituTableViewCell;
@protocol MeituTableViewCellDelegate <NSObject>

- (void)initCell:(MeituTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath;

@end

@interface MeituTableViewCell : UITableViewCell
@property(nonatomic,weak)UIImageView *iv;
@property(nonatomic,strong)NSIndexPath *indexPath;
@property(nonatomic,weak)id<MeituTableViewCellDelegate> delegate;

+ (instancetype)cellWithTableView:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath andMeitu:(Meitu *)mt andDelegate:(id<MeituTableViewCellDelegate>) delegate;
@end
