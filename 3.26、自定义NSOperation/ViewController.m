//
//  ViewController.m
//  3.26、自定义NSOperation
//
//  Created by 李坤 on 16/12/16.
//
//

#import "ViewController.h"
#import "Meitu.h"
#import "MeituTableViewCell.h"
#import "DownLoadOperation.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource,MeituTableViewCellDelegate,DownLoadOperationDelegate,UIScrollViewDelegate>
//数据是否已经加载
@property(nonatomic,assign)Boolean isLoad;
//tableView对象
@property(nonatomic,weak)UITableView *tv;
//数据列表
@property(nonatomic,copy)NSMutableArray<Meitu *> *mts;
//url和图像对象字典
@property(nonatomic,copy)NSMutableDictionary<NSString *,UIImage *> *imgsDic;
//url和NSOperation对象字典
@property(nonatomic,copy)NSMutableDictionary<NSString *,DownLoadOperation *> *imgDloDic;
//NSOperationQueue队列
@property(nonatomic,strong)NSOperationQueue *queue;
@end

@implementation ViewController

- (NSMutableArray<Meitu *> *)mts{
    if(_mts==nil){
        self->_mts = [NSMutableArray array];
        [self addData];
    }
    return self->_mts;
}

- (void)addData{
    for (int i=1; i<=20; i++) {
        [self->_mts addObject:[Meitu meituWithTitle:[NSString stringWithFormat:@"自定义NSOperation-%i",i] andImgUrl:[NSString stringWithFormat:@"https://test1119-static.yijiedai.org/iosimgs/%i.jpg",i]]];
        [self.tv reloadData];
        self.isLoad = NO;
    }
}

- (void)initCell:(MeituTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath;{
    
    Meitu *mt = self.mts[indexPath.row];
    UIImage *image = self.imgsDic[mt.imgUrl];
    if(image==nil){
        DownLoadOperation *dlo = self.imgDloDic[mt.imgUrl];
        if(dlo==nil){
            DownLoadOperation *operate = [DownLoadOperation dpdWithImgUrl:mt.imgUrl andIndexPath:indexPath andDelegate:self];
            [self.queue addOperation:operate];
            self.imgDloDic[mt.imgUrl]=operate;
        }
    }else{
        cell.iv.image = image;
    }
    
}

- (NSMutableDictionary<NSString *,DownLoadOperation *> *)imgDloDic{
    if(self->_imgDloDic==nil){
        self->_imgDloDic = [[NSMutableDictionary alloc] init];
    }
    return _imgDloDic;
}

- (NSMutableDictionary<NSString *,UIImage *> *)imgsDic{
    if(_imgsDic==nil){
        _imgsDic = [[NSMutableDictionary alloc] init];
    }
    return _imgsDic;
}

- (void)downLoadOperation:(DownLoadOperation *)operation didFinishDownLoad:(UIImage *)image{
    self.imgsDic[operation.imgUrl] = image;
    MeituTableViewCell *cell = [self.tv cellForRowAtIndexPath:operation.indexPath];
    cell.iv.image = image;
//    NSLog(@"%@---%@---%@",cell,self.mts[operation.indexPath.row].imgUrl,operation.indexPath);
}

- (NSOperationQueue *)queue{
    if(_queue==nil){
        _queue = [[NSOperationQueue alloc] init];
    }
    return _queue;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [MeituTableViewCell cellWithTableView:tableView andIndexPath:indexPath andMeitu:self.mts[indexPath.row] andDelegate:self];
//    NSLog(@"%@",cell);
    return cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isLoad = NO;
    UITableView *tv = [[UITableView alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height, self.view.frame.size.width, self.view.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height) style:UITableViewStylePlain];
    tv.dataSource = self;
    tv.delegate = self;
    [tv setRowHeight:220];
    [self.view addSubview:tv];
    self.tv = tv;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(self.tv.contentOffset.y+self.tv.bounds.size.height>=self.tv.contentSize.height-100){
        if(!self.isLoad){
            //加载数据
            self.isLoad = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW,(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSLog(@"加载数据");
                [self addData];
            });
        }
        NSLog(@"哈哈哈");
    }
}
@end
