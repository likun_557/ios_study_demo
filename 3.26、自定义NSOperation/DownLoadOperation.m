//
//  DownLoadOperation.m
//  iOS
//
//  Created by 李坤 on 16/12/16.
//
//

#import "DownLoadOperation.h"

@interface DownLoadOperation ()

@end

@implementation DownLoadOperation

- (instancetype)initWithImgUrl:(NSString *)imgUrl andIndexPath:(NSIndexPath *)indexPath andDelegate:(id<DownLoadOperationDelegate>)delegate{
    if(self = [super init]){
        self.imgUrl = imgUrl;
        self.indexPath = indexPath;
        self.delegate = delegate;
    }
    return self;
}

+ (instancetype)dpdWithImgUrl:(NSString *)imgUrl andIndexPath:(NSIndexPath *)indexPath andDelegate:(id<DownLoadOperationDelegate>)delegate{
    return [[self alloc] initWithImgUrl:imgUrl andIndexPath:indexPath andDelegate:delegate];
}

- (void)main{
    NSURL *url = [NSURL URLWithString:self.imgUrl];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *img = [UIImage imageWithData:data];
    dispatch_async(dispatch_get_main_queue(), ^{
        if([self.delegate respondsToSelector:@selector(downLoadOperation:didFinishDownLoad:)]){
            [self.delegate downLoadOperation:self didFinishDownLoad:img];
        }
    });
}

@end
