//
//  MeituTableViewCell.m
//  iOS
//
//  Created by 李坤 on 16/12/16.
//
//

#import "MeituTableViewCell.h"
#import "Meitu.h"

@interface MeituTableViewCell()
@property(nonatomic,strong)Meitu *mt;
@end

@implementation MeituTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath andMeitu:(Meitu *)mt andDelegate:(id<MeituTableViewCellDelegate>)delegate{
    static NSString *identifier = @"metuCell";
    MeituTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell==nil){
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.mt = mt;
    cell.indexPath = indexPath;
    cell.delegate = delegate;
    if([cell.delegate respondsToSelector:@selector(initCell:withIndexPath:)]){
        [cell.delegate initCell:cell withIndexPath:indexPath];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    UIImageView *iv = [[UIImageView alloc] init];
    iv.frame = CGRectMake(10, 10, [UIScreen mainScreen].bounds.size.width-20, 200);
//    [UIScreen mainScreen].bounds
    iv.clipsToBounds = YES;
    iv.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:iv];
    self.iv = iv;
    return self;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"self = %p, frame = %@, indexPath = %lu",self,NSStringFromCGRect(self.frame),self.indexPath.row];
}

@end
