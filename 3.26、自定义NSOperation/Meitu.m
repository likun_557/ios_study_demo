//
//  Meitu.m
//  iOS
//
//  Created by 李坤 on 16/12/16.
//
//

#import "Meitu.h"

@implementation Meitu

- (instancetype)initWithTitle:(NSString *)title andImgUrl:(NSString *)imgUrl{
    if(self = [super init]){
        self.title = title;
        self.imgUrl = imgUrl;
    }
    return self;
}

+ (instancetype)meituWithTitle:(NSString *)title andImgUrl:(NSString *)imgUrl{
    return [[self alloc] initWithTitle:title andImgUrl:imgUrl];
}

@end
