//
//  DownLoadOperation.h
//  iOS
//
//  Created by 李坤 on 16/12/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class DownLoadOperation;

@protocol DownLoadOperationDelegate <NSObject>

- (void)downLoadOperation:(DownLoadOperation *)operation didFinishDownLoad:(UIImage *)image;

@end

@interface DownLoadOperation : NSOperation

@property(nonatomic,copy)NSString *imgUrl;

@property(nonatomic,strong)NSIndexPath *indexPath;

@property(nonatomic,weak)id<DownLoadOperationDelegate> delegate;

- (instancetype)initWithImgUrl:(NSString *)imgUrl andIndexPath:(NSIndexPath *)indexPath andDelegate:(id<DownLoadOperationDelegate>) delegate;

+ (instancetype)dpdWithImgUrl:(NSString *)imgUrl andIndexPath:(NSIndexPath *)indexPath andDelegate:(id<DownLoadOperationDelegate>) delegate;

@end
