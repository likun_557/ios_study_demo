//
//  AppDelegate.h
//  1.13、xib
//
//  Created by 李坤 on 16/12/21.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

