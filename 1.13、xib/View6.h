//
//  View6.h
//  iOS
//
//  Created by 李坤 on 16/12/21.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface View6 : NSObject
@property (strong, nonatomic) IBOutlet UIView *view0;

+ (id)viewFromNib;

@end
