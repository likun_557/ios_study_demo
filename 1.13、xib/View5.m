//
//  View5.m
//  iOS
//
//  Created by 李坤 on 16/12/21.
//
//

#import "View5.h"

@interface View5 ()


@end

@implementation View5

+ (instancetype)viewFromNib{
//    return [NSBound main]
    NSArray *arry = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    return arry[0];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
