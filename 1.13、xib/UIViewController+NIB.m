//
//  UIViewController+NIB.m
//  iOS
//
//  Created by 李坤 on 16/12/21.
//
//

#import "UIViewController+NIB.h"

@implementation UIViewController (NIB)

+ (instancetype)loadFromNib{
    Class cs = [self class];
    return [[cs alloc] initWithNibName:NSStringFromClass(cs) bundle:[NSBundle mainBundle]];
}

@end
