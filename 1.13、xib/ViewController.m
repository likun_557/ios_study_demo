//
//  ViewController.m
//  1.13、xib
//
//  Created by 李坤 on 16/12/21.
//
//

#import "ViewController.h"
#import "View3.h"
#import "View4.h"
#import "View5.h"
#import "View6.h"
#import "View7.h"
#import "UIViewController+NIB.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *blueView;

@property(nonatomic,strong)View3 *v3;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self m7];
}

- (void)m7{
    View7 *view = [View7 loadFromNib];
    UIView *view0 = view.view;
    view0.frame = CGRectMake(10, 20, self.view.bounds.size.width-20, 100);
    [self.view addSubview:view0];
    
    NSLog(@"%@",view.view1);
    
    
}

- (void)m6{
    UIView *view0 = [View6 viewFromNib];
    view0.frame = CGRectMake(10, 20, self.view.bounds.size.width-20, 100);
    [self.view addSubview:view0];
    NSLog(@"%@",view0);
}

- (void)m5{
    View5 *view0 = [View5 viewFromNib];
    view0.frame = CGRectMake(10, 20, self.view.bounds.size.width-20, 100);
    [self.view addSubview:view0];
    NSLog(@"%@",view0);
}

- (void)m4{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"view4" owner:nil options:nil];
    View4 *view0 = array[0];
    view0.frame = CGRectMake(10, 20, self.view.bounds.size.width-20, 100);
    [self.view addSubview:view0];
    NSLog(@"%@",view0.view1);
}

- (void)m3{
    self.v3 = [[View3 alloc] init];
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"view3" owner:self.v3 options:nil];
    self.blueView = self.v3.blueView;
    self.blueView.frame = CGRectMake(10, 20, self.view.bounds.size.width-20, 100);
    [self.view addSubview:self.blueView];
    NSLog(@"%i",array[0]==self.v3.blueView);
    NSLog(@"%@",array);
}

- (void)m2{
    [[NSBundle mainBundle] loadNibNamed:@"view2" owner:self options:nil];
    
//    UIView *view0 = array[0];
    self.blueView.frame = CGRectMake(10, 20, self.view.bounds.size.width-20, 100);
    [self.view addSubview:self.blueView];
    NSLog(@"%@",self.blueView.subviews);
}

- (void)m1{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"view1" owner:nil options:nil];
    UIView *view0 = array[0];
    view0.frame = CGRectMake(10, 20, self.view.bounds.size.width-20, 100);
    [self.view addSubview:view0];
    NSLog(@"%@",array);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
