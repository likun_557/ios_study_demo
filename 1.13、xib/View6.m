//
//  View6.m
//  iOS
//
//  Created by 李坤 on 16/12/21.
//
//

#import "View6.h"

@interface View6 ()

@end

@implementation View6
+ (id)viewFromNib{
    Class a = [self class];
    id tar = [[a alloc] init];
    UIView *view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(a) owner:tar options:nil][0];
    NSLog(@"%@,%@",view,[tar view0]);
    return view;
}
@end
