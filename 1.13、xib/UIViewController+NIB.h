//
//  UIViewController+NIB.h
//  iOS
//
//  Created by 李坤 on 16/12/21.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (NIB)
+ (instancetype)loadFromNib;
@end
