//
//  AppDelegate.h
//  3.14、编码启动UIApplication
//
//  Created by 李坤 on 16/12/11.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

