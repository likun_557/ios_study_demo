//
//  ViewController.m
//  3.14、编码启动UIApplication
//
//  Created by 李坤 on 16/12/11.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *btn = [[UIButton alloc] init];
    btn.frame = CGRectMake(100, 100, 100, 30);
    btn.backgroundColor = [UIColor redColor];
    btn.titleLabel.textColor = [UIColor whiteColor];
    [btn setTitle:@"点击我" forState:UIControlStateNormal];
    [self.view addSubview:btn];
    NSLog(@"哈哈哈");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
