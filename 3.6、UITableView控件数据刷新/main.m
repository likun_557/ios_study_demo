//
//  main.m
//  3.6、UITableView控件数据刷新
//
//  Created by 李坤 on 16/12/7.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
