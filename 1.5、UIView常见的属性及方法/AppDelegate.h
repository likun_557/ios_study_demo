//
//  AppDelegate.h
//  1.5、UIView常见的属性及方法
//
//  Created by 李坤 on 16/11/27.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

