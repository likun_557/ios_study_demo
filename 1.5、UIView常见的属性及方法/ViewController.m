//
//  ViewController.m
//  1.5、UIView常见的属性及方法
//
//  Created by 李坤 on 16/11/27.
//
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *delBtn;
@property (weak, nonatomic) IBOutlet UIButton *subListBtn;
@property (weak,nonatomic) UILabel *label;

@end

@implementation ViewController
{
    BOOL isAdd;
}
/**
 *  添加一个label
 */
- (IBAction)addLabel{

    if(!isAdd){
        //创建一个label
        UILabel *label = [[UILabel alloc] init];
        //设置labe的背景颜色
        label.backgroundColor = [UIColor blueColor];
        //设置label的文字
        label.text = @"我是一个UILabel";
        //设置label中文件的颜色
        label.textColor = [UIColor whiteColor];
        //设置label的位置以及大小
        label.frame = CGRectMake(100, 100, 200, 100);
        //设置label中文字的对其方式
        label.textAlignment = NSTextAlignmentCenter;
        self.label = label;
        //将labe添加到当前control的view视图中
        [self.view addSubview:self.label];
        self->isAdd = YES;
        
        NSLog(@"%@",NSStringFromCGRect(label.frame));
    }
}

/**
 *  删除添加好的label
 */
- (IBAction)delLable{
    if(isAdd){
        //将label从父视图中删除
        [self.label removeFromSuperview];
        isAdd = NO;
    }
}

/**
 *  显示所有子view
 */
- (IBAction)showSubViews{
    NSArray<UIView *> *subviews = self.view.subviews;
    for (UIView *v1 in subviews) {
        NSLog(@"%@",v1);
    }
}


/**
 *  view加载好了之后将调用该方法
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    isAdd = NO;
}


/**
 *  当前应用导致内存发生警告时将触发该方法
 */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
