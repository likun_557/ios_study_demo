//
//  main.m
//  1.5、UIView常见的属性及方法
//
//  Created by 李坤 on 16/11/27.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
