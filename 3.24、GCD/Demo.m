//
//  Demo.m
//  iOS
//
//  Created by 李坤 on 16/12/15.
//
//

#import "Demo.h"

@implementation Demo

- (void)once:(NSObject *)target withSel:(SEL)sel{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if([target respondsToSelector:sel]){
            [target performSelector:sel];
        }
    });
}

@end
