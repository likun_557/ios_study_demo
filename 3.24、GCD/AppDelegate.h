//
//  AppDelegate.h
//  3.24、GCD
//
//  Created by 李坤 on 16/12/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

