//
//  Demo.h
//  iOS
//
//  Created by 李坤 on 16/12/15.
//
//

#import <Foundation/Foundation.h>

@interface Demo : NSObject

- (void)once:(NSObject *)target withSel:(SEL)sel;

@end
