//
//  FrameUtil.m
//  iOS
//
//  Created by 李坤 on 16/12/12.
//
//

#import "FrameUtil.h"

@implementation FrameUtil

+ (void)initBtn:(UIButton *)btn andBck:(UIColor *)backgroundColor andTitle:(NSString *)title andTitleColor:(UIColor *)titleColor{
    btn.backgroundColor = backgroundColor;
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.textColor = titleColor;
}

+ (void)initBtnDefaultStyle:(UIButton *)btn andTitle:(NSString *)title{
    [self initBtn:btn andBck:[UIColor redColor] andTitle:title andTitleColor:[UIColor whiteColor]];
}
@end
