//
//  main.m
//  3.17、UINavigationController的属性和基本使用
//
//  Created by 李坤 on 16/12/12.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
