//
//  FrameUtil.h
//  iOS
//
//  Created by 李坤 on 16/12/12.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FrameUtil : NSObject

+ (void)initBtn:(UIButton *)btn andBck:(UIColor *)backgroundColor andTitle:(NSString *)title andTitleColor:(UIColor *)titleColor;

+ (void)initBtnDefaultStyle:(UIButton *)btn andTitle:(NSString *)title;
@end
