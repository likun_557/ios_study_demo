//
//  ViewController2.m
//  iOS
//
//  Created by 李坤 on 16/12/12.
//
//

#import "ViewController2.h"
#import "ViewController3.h"

@interface ViewController2 ()

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"我是控制器2";
//    UIBarButtonItem *b1 = [[UIBarButtonItem alloc] initWithTitle:@"bar1" style:UIBarButtonItemStylePlain target:self action:nil];
//    UIBarButtonItem *b2 = [[UIBarButtonItem alloc] initWithTitle:@"bar2" style:UIBarButtonItemStylePlain target:self action:nil];
//    UIBarButtonItem *b3 = [[UIBarButtonItem alloc] initWithTitle:@"bar3" style:UIBarButtonItemStylePlain target:self action:nil];
//    self.navigationItem.rightBarButtonItems = @[b1,b2,b3];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"后退" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem.title = @"后退";
    
    UIButton *btn = [[UIButton alloc] init];
    btn.frame = CGRectMake(50, 100, self.view.bounds.size.width-100, 30);
    [btn setTitle:@"跳转到下一个控制器" forState:UIControlStateNormal];
    btn.titleLabel.textColor = [UIColor whiteColor];
    btn.backgroundColor = [UIColor redColor];
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}

- (void)btnClick{
    Class v = NSClassFromString(@"ViewController3");
    id vct = [[v alloc] init];
    [self.navigationController pushViewController:vct animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
