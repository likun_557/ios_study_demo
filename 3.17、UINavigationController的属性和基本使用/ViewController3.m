//
//  ViewController3.m
//  iOS
//
//  Created by 李坤 on 16/12/12.
//
//

#import "ViewController3.h"
#import "FrameUtil.h"
@interface ViewController3 ()

@end

@implementation ViewController3

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"我是控制器2";
    
    CGFloat padding = 10;
    int tageIndex = 1;
    
    //创建按钮
    UIButton *btn1 = [[UIButton alloc] init];
    btn1.frame = CGRectMake(50, 100, self.view.bounds.size.width-100, 30);
    [FrameUtil initBtnDefaultStyle:btn1 andTitle:@"返回上一个controller"];
    //添加按钮点击事件
    [btn1 addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    btn1.tag = tageIndex++;
    [self.view addSubview:btn1];
    
    UIButton *btn2 = [[UIButton alloc] init];
    btn2.frame = CGRectMake(btn1.frame.origin.x, CGRectGetMaxY(btn1.frame)+padding, self.view.bounds.size.width-100, 30);
    [FrameUtil initBtnDefaultStyle:btn2 andTitle:@"返回根controller"];
    [btn2 addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    btn2.tag = tageIndex++;
    [self.view addSubview:btn2];
}

- (void)onClick:(UIView *)view{
    if(view.tag==1){
        //返回上一个controller
        [self.navigationController popViewControllerAnimated:YES];
    }else if(view.tag == 2){
        //返回跟controller
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
