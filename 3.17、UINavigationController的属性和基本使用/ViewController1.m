//
//  ViewController1.m
//  iOS
//
//  Created by 李坤 on 16/12/12.
//
//

#import "ViewController1.h"

@interface ViewController1 ()

@end

@implementation ViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",[UIApplication sharedApplication].keyWindow.subviews[0]);
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"我是控制器1";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"后退" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    UIButton *btn = [[UIButton alloc] init];
    btn.frame = CGRectMake(50, 100, self.view.bounds.size.width-100, 30);
    [btn setTitle:@"跳转到下一个控制器" forState:UIControlStateNormal];
    btn.titleLabel.textColor = [UIColor whiteColor];
    btn.backgroundColor = [UIColor redColor];
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}

- (void)btnClick{
    Class v = NSClassFromString(@"ViewController2");
    id vct = [[v alloc] init];
    [self.navigationController pushViewController:vct animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//视图将要显示的时候调用
- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"%s",__func__);
}

//视图已经显示的时候调用
- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"%s",__func__);
}
//视图将要消失的时候调用
- (void)viewWillDisappear:(BOOL)animated{
    NSLog(@"%s",__func__);
}

//视图已经消失的时候调用
- (void)viewDidDisappear:(BOOL)animated{
//    [super viewDidDisappear:animated];
    NSLog(@"%s",__func__);
}

- (void)viewWillUnload{
    NSLog(@"%s",__func__);
}

- (void)viewDidUnload{
    NSLog(@"%s",__func__);
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
