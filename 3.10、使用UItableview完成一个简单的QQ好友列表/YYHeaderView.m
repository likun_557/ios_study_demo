//
//  YYHeaderView.m
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import "YYHeaderView.h"
#import "YYQQGroupModel.h"

@interface YYHeaderView ()
@property(nonatomic,strong)UIButton *btn;
@property(nonatomic,strong)UILabel *lab;
@end

@implementation YYHeaderView

+ (instancetype)headerWithTableView:(UITableView *)tableView{
    static NSString *identifier = @"header";
    //先从缓存中去拿数据
    YYHeaderView *headerView = (YYHeaderView *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(headerView==nil){
        headerView = [[self alloc] initWithReuseIdentifier:identifier];
    }
    return headerView;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    //初始化父类中的构造方法
    if(self = [super initWithReuseIdentifier:reuseIdentifier]){
        //创建一个按钮
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        //设置按钮的属性
        //设置普通状态下按钮的背景图片
        [btn setBackgroundImage:[UIImage imageNamed:@"buddy_header_bg"] forState:UIControlStateNormal];
        //设置高亮状态下按钮的背景图片
        [btn setBackgroundImage:[UIImage imageNamed:@"buddy_header_bg_highlighted"] forState:UIControlStateSelected];
        //设置按钮上面的倒三角
        [btn setImage:[UIImage imageNamed:@"buddy_header_arrow"] forState:UIControlStateNormal];
        //设置按钮上信息的对其方式为左对齐
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //设置按钮上文字距离小三角图片的距离
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
        //设置按钮上分组标题的文本颜色（颜色是白色的）
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        //添加按钮的点击事件
        [btn addTarget:self action:@selector(btnOnclick:) forControlEvents:UIControlEventTouchUpInside];
        
        //设置btn中图片不填充整个imageview
        btn.imageView.contentMode = UIViewContentModeLeft;
        
        //超出范围图片不要剪切
        btn.imageView.clipsToBounds = YES;
//        btn.imageView.layer.masksToBounds = YES;
        
        //把按钮添加到视图
        [self addSubview:btn];
        self.btn = btn;
        
        //创建一个lab
        UILabel *lab = [[UILabel alloc] init];
        //设置在线人数的对齐方式为右对齐
        lab.textAlignment = NSTextAlignmentRight;
        //设置在线人数的文字颜色为灰色
        lab.textColor = [UIColor grayColor];
        [self addSubview:lab];
        self.lab = lab;
    }
    return self;
}

- (void)btnOnclick:(UIButton *)btn{
//    NSLog(@"按钮被点击了");
    //修改模型的isopen属性
    //1.修改模型数据
    self.group.open = !self.group.isOpen;
    //2.刷新表格
    //刷新表格的功能由控制器完成，在这里可以设置一个代理，当按钮被点击的时候，通知代理堆表格进行刷新
    //通知代理
    if([self.delegate respondsToSelector:@selector(headerViewDidClickHeaderView:)]){
        [self.delegate performSelector:@selector(headerViewDidClickHeaderView:) withObject:self];
    }
}

//当控件的frame值被改变的时候，会自动调用改方法，故可以在此方法中设置空间的frame
- (void)layoutSubviews{
    #warning 一定不要忘记调用父类的方法
    [super layoutSubviews];
    //设置按钮的frame和头部视图一样大小
    self.btn.frame = self.bounds;
    
    //设置lab的frame
    CGFloat padding = 20;
    CGFloat labW = 50;
    CGFloat labH = self.frame.size.height;
    CGFloat labY = 0;
    CGFloat labX = self.frame.size.width - padding - labW;
    self.lab.frame = CGRectMake(labX, labY, labW, labH);
}

#pragma mark - 当一个空间被添加到其他视图上的时候会调用以下方法
//已经被添加到视图上的时候会调用
- (void)didMoveToSuperview{
    
//    NSLog(@"已经添加到视图了");
    //在这个方法中就快要拿到最新的被添加到tableview上的头部视图修改它的图片
    if(self.group.isOpen){
        //让小三角图片向下旋转
        self.btn.imageView.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
}

//即将被添加到父视图上的时候会调用
- (void)willMoveToSuperview:(UIView *)newSuperview{
//    NSLog(@"将要添加到视图了");
}

//重写set方法，设置数据
- (void)setGroup:(YYQQGroupModel *)group{
    _group = group;
    //设置分组标题
    self.btn.titleLabel.text = _group.name;
    #warning 请注意在设置按钮的文本的时候，一定要设置按钮的状态，像上面这样设置不会显示
    [self.btn setTitle:_group.name forState:UIControlStateNormal];
    //设置在线人数
    self.lab.text = [NSString stringWithFormat:@"%@/%lu",_group.online,_group.friends.count];
}


@end
