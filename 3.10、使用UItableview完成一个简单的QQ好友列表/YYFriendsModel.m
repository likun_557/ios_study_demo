//
//  YYFriendsModel.m
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import "YYFriendsModel.h"

@implementation YYFriendsModel

- (instancetype)initWithDict:(NSDictionary *)dict{
    if(self = [super init]){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)friendsWithDict:(NSDictionary *)dict{
    return [[self alloc] initWithDict:dict];
}

@end
