//
//  ViewController.m
//  3.10、使用UItableview完成一个简单的QQ好友列表
//
//  Created by 李坤 on 16/12/8.
//
//

#import "ViewController.h"
#import "YYQQGroupModel.h"
#import "YYfriendCell.h"
#import "YYHeaderView.h"
@interface ViewController ()<YYHeaderViewDelegate,UITableViewDataSource,UITableViewDelegate>
/**
 *  用来保存所有分组数据
 */
@property(nonatomic,strong)NSArray *groupFriends;

@property(nonatomic,weak)UITableView *tableView;
@end

@implementation ViewController
- (NSArray *)groupFriends{
    if(_groupFriends == nil){
        NSString *fullpath = [[NSBundle mainBundle] pathForResource:@"friends.plist" ofType:nil];
        NSArray *arrayM = [NSArray arrayWithContentsOfFile:fullpath];
        NSMutableArray *models = [NSMutableArray arrayWithCapacity:arrayM.count];
        for (NSDictionary *dict in arrayM) {
            YYQQGroupModel *group = [YYQQGroupModel qqGroupWithDict:dict];
            [models addObject:group];
        }
        _groupFriends = [models copy];
    }
    return _groupFriends;
}

#pragma mark - 设置数据源
//返回多少组
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
     NSLog(@"%lu",self.groupFriends.count);
    return self.groupFriends.count;
}

//返回每组中的行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //取出对应的组模型
    YYQQGroupModel *group = self.groupFriends[section];
    //在这里进行判断，如果该组收拢，那就返回0，如果改组打开，就返回实际行数
    NSLog(@"group.isOpen = %i",group.isOpen);
    if(group.isOpen){
        //代表要展开
        return group.friends.count;
    }else{
        //代表要合拢
        return 0;
//        return group.friends.count;
    }
}

//每组中每行的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //1.创建cell
    YYfriendCell *cell = [YYfriendCell cellWithTableView:tableView];
    //2.设置cell
    YYQQGroupModel *group = self.groupFriends[indexPath.section];
    YYFriendsModel * friends = group.friends[indexPath.row];
    cell.friends = friends;
    return cell;
}

#pragma mark - 代理方法
//当一个分组标题进入视野的时候就会调用该方法
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    NSLog(@"%s",__func__);
//    //1.创建头部视图
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor grayColor];
    
    //创建自定义的头部视图
    YYHeaderView *headerview = [YYHeaderView headerWithTableView:tableView];
    //设置当前控制器为代理
    headerview.delegate = self;
    //设置头部视图的数据
    YYQQGroupModel *groupmodel = self.groupFriends[section];
    headerview.group = groupmodel;
    //返回头部视图
    return headerview;
}

//设置分组头部标题的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return (NSArray *)@[@"小学",@"初中",@"高中",@"大学"];
}

- (void)headerViewDidClickHeaderView:(YYHeaderView *)headerView{
    NSLog(@"xxx");
    //重写调用数据源的方法刷新数据
    [self.tableView reloadData];
    NSLog(@"哈哈哈哈");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat padding = 30;
    UITableView *tv = [[UITableView alloc] initWithFrame:CGRectMake(0, padding, self.view.bounds.size.width, self.view.bounds.size.height-padding)];
    tv.delegate = self;
    tv.dataSource = self;
    [self.view addSubview:tv];
    self.tableView = tv;
   
}

//是否隐藏状态栏
//- (BOOL)prefersStatusBarHidden{
//    return YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
