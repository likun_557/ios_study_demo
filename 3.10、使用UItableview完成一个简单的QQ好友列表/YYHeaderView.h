//
//  YYHeaderView.h
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import <UIKit/UIKit.h>
@class YYQQGroupModel,YYHeaderView;

/**
 *  定义一个header的协议
 */
@protocol YYHeaderViewDelegate <NSObject>
/**
 *  点击headerview的时候调用
 *
 *  @param headerView headerview对象
 */
- (void)headerViewDidClickHeaderView:(YYHeaderView *)headerView;

@end
@interface YYHeaderView : UITableViewHeaderFooterView
@property(nonatomic,strong)YYQQGroupModel *group;

//提供一个方法，创建tableview的视图
+ (instancetype)headerWithTableView:(UITableView *)tableView;

//delete遵守YYHeaderViewDelegate协议，可以使用YYHeaderViewDelegate协议中的方法
@property(nonatomic,weak)id<YYHeaderViewDelegate> delegate;

@end
