//
//  main.m
//  3.10、使用UItableview完成一个简单的QQ好友列表
//
//  Created by 李坤 on 16/12/8.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
