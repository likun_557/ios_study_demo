//
//  YYQQGroupModel.h
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import <Foundation/Foundation.h>

@interface YYQQGroupModel : NSObject
/**
 *  名称属性
 */
@property(nonatomic,copy)NSString *name;

/**
 *  是否在线
 */
@property(nonatomic,copy)NSString *online;

/**
 *  好友列表
 */
@property(nonatomic,strong)NSArray *friends;

/**
 *  记录当前组是否需要打开
 */
@property(nonatomic,assign,getter=isOpen)BOOL open;

- (instancetype)initWithDict:(NSDictionary *)dict;

+ (instancetype)qqGroupWithDict:(NSDictionary *)dict;
@end
