//
//  YYfriendCell.h
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import <UIKit/UIKit.h>
@class YYFriendsModel;

@interface YYfriendCell : UITableViewCell

@property(nonatomic,strong)YYFriendsModel *friends;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
