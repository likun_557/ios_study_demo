//
//  YYfriendCell.m
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import "YYfriendCell.h"
#import "YYFriendsModel.h"

@interface YYfriendCell ()

@end

@implementation YYfriendCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *identifier = @"qq";
    YYfriendCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell==nil){
        cell = [[self alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    return cell;
}

- (void)setFriends:(YYFriendsModel *)friends{
    _friends = friends;
    //1.设置头像
    self.imageView.image = [UIImage imageNamed:_friends.icon];
    //2.设置昵称
    self.textLabel.text = _friends.name;
    //3.设置间接
    self.detailTextLabel.text = _friends.intro;
    //判断是否是会员
    /**
     *  这里有个注意点，如果不写else设置为黑色会怎么样？
     */
    if(_friends.isVip){
        self.textLabel.textColor = [UIColor redColor];
    }else{
        self.textLabel.textColor = [UIColor blackColor];
    }
}

@end
