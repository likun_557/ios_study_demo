//
//  YYFriendsModel.h
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import <Foundation/Foundation.h>

@interface YYFriendsModel : NSObject
/**
 *  每个好友的名称
 */
@property(nonatomic,copy)NSString *name;
/**
 *  每个好友的头像
 */
@property(nonatomic,copy)NSString *icon;
/**
 *  每个好友的个性签名
 */
@property(nonatomic,copy)NSString *intro;
/**
 *  该好友是否是vip
 */
@property(nonatomic,assign,getter=isVip)BOOL vip;

- (instancetype)initWithDict:(NSDictionary *)dict;

+ (instancetype)friendsWithDict:(NSDictionary *)dict;
@end
