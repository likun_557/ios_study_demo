//
//  YYQQGroupModel.m
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import "YYQQGroupModel.h"
#import "YYFriendsModel.h"
@implementation YYQQGroupModel

- (instancetype)initWithDict:(NSDictionary *)dict{
    if(self = [super init]){
        //将字典转换为模型
        [self setValuesForKeysWithDictionary:dict];
        
        //定义一个数组来保存转换后的模型
        NSMutableArray *models = [NSMutableArray arrayWithCapacity:self.friends.count];
        for (NSDictionary *dict in self.friends) {
            YYFriendsModel *friends = [YYFriendsModel friendsWithDict:dict];
            [models addObject:friends];
        }
        _friends = [models copy];
    }
    return self;
}

+ (instancetype)qqGroupWithDict:(NSDictionary *)dict{
    return [[self alloc] initWithDict:dict];
}

@end
