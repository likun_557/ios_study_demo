//
//  AppDelegate.h
//  1.4、父控件和子控件的理解
//
//  Created by 李坤 on 16/11/25.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

