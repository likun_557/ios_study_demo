//
//  ViewController1.m
//  iOS
//
//  Created by 李坤 on 16/12/11.
//
//

#import "ViewController1.h"
#import "ViewController2.h"
@interface ViewController1 ()

@end

@implementation ViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"第一个界面";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"呵呵呵" style:UIBarButtonItemStylePlain target:nil action:nil];
//    self.view.frame = self.view.window.bounds;
    UIButton *btn = [[UIButton alloc] init];
//    self.view.window
    NSLog(@"%s--%@",__func__,NSStringFromCGRect(self.view.frame));
    btn.frame = CGRectMake(50, 100, self.view.bounds.size.width - 100, 30);
    [btn setTitle:@"我是第1个，跳转到第2个" forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor redColor];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(toNextControlller) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}

- (void)toNextControlller{
    
    [self.navigationController pushViewController:[[ViewController2 alloc] init] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
