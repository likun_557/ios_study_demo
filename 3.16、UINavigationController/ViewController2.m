//
//  ViewController2.m
//  iOS
//
//  Created by 李坤 on 16/12/11.
//
//

#import "ViewController2.h"
#import "ViewController3.h"

@interface ViewController2 ()

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    NSLog(@"----%@",NSStringFromCGRect(self.view.frame));
    NSLog(@"%s--%@",__func__,NSStringFromCGRect(self.view.frame));
    UIButton *btn = [[UIButton alloc] init];
    btn.frame = CGRectMake(50, 100, self.view.bounds.size.width - 100, 30);
    [btn setTitle:@"我是第2个，跳转到第3个" forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor redColor];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(toNextControlller) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}

- (void)toNextControlller{
    
    [self.navigationController pushViewController:[[ViewController3 alloc] init] animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
