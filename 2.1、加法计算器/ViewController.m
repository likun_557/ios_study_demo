//
//  ViewController.m
//  2.1、加法计算器
//
//  Created by 李坤 on 16/11/27.
//
//

#import "ViewController.h"

@interface ViewController ()<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *num1Field;
@property (weak, nonatomic) IBOutlet UITextField *num2Field;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@end

@implementation ViewController

/**
 *  alterview被点击的时候调用的方法
 *
 *  @param alertView   alterview对象
 *  @param buttonIndex 那个按钮
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%@",alertView.subviews);
    NSLog(@"%@,%lu",alertView,buttonIndex);
}

#pragma mark -计算结果
- (IBAction)calculate{
    //1.拿到用户输入的第一个数字
    if(self.num1Field.text.length == 0){
//        NSLog(@"第一个数字不能为空!");//此处打印提示信息，用户是看不到的
        [self showError:@"第一个数字不能为空!"];
        return;
    }
    NSInteger num1 = [self.num1Field.text integerValue];
    //2.拿到用户输入的第二个数字
    if(self.num2Field.text.length == 0){
        [self showError:@"第二个数字不能为空!"];
        return;
    }
    NSInteger num2 = [self.num2Field.text integerValue];
    //3.计算结果
    NSInteger result = num1 + num2;
    //4.将结果显示在label上面
    self.resultLabel.text = [NSString stringWithFormat:@"%lu",result];
}

/**
 *  展示错误信息
 *
 *  @param error 错误信息内容
 */
- (void)showError:(NSString *)error{
    [[[UIAlertView alloc] initWithTitle:@"错误提示" message:error delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil] show];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%s",__func__);
}

@end
