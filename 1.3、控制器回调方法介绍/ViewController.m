//
//  ViewController.m
//  1.3、控制器回调方法介绍
//
//  Created by 李坤 on 16/11/25.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

/**
 *  当view将要出现的时候调用
 *
 *  @param animated
 */
- (IBAction)addLabel:(id)sender {
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"%s",__func__);
}

/**
 *  当view已经显示的时候调用
 *
 *  @param animated
 */
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"%s",__func__);
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    NSLog(@"%s",__func__);
}
/**
 *  当控制器已经加载好的时候调用
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"%s",__func__);
}

/**
 *  当内存发生警告的时候调用
 */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"%s",__func__);
}

@end
