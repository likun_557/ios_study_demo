//
//  ViewController3.m
//  iOS
//
//  Created by 李坤 on 16/12/13.
//
//

#import "ViewController3.h"

@interface ViewController3 ()

@property (weak, nonatomic) IBOutlet UILabel *lab;
@property(nonatomic,strong)UIButton *btn;
@end

@implementation ViewController3

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
    self.lab.backgroundColor = [UIColor redColor];
    self.lab.textColor = [UIColor whiteColor];
    self.lab.text = @"我是控制器3";
    self.lab.frame = CGRectMake(10, 100, self.view.frame.size.width - 20, 40);
    self.btn = [[UIButton alloc] init];
    self.btn.frame =CGRectMake(10, 150, self.view.frame.size.width - 20, 40);
    self.btn.backgroundColor = [UIColor redColor];
    [self.btn setTitle:@"改变label长度" forState:UIControlStateNormal];
    [self.btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btn];
    
}

- (void)click{
    NSLog(@"%@",NSStringFromCGRect(self.lab.frame));
    #warning 这边原来设置的是bounds 我改为frame了
    self.lab.frame = CGRectMake(10, 100, self.view.frame.size.width - 20, 30);
    self.lab.backgroundColor = [UIColor redColor];
    self.lab.textColor = [UIColor whiteColor];
    self.lab.text = @"我是控制器3";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
