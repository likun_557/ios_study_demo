//
//  TabBarController1.m
//  iOS
//
//  Created by 李坤 on 16/12/13.
//
//

#import "TabBarController1.h"

@interface TabBarController1 ()

@end

@implementation TabBarController1

- (void)loadView{
    NSLog(@"----start %s",__func__);
    [super loadView];
    NSLog(@"----end %s",__func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"----%s",__func__);
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"----%s",__func__);
}

- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"----%s",__func__);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
