//
//  ViewController.m
//  3.20、UITabBarController简单介绍
//
//  Created by 李坤 on 16/12/13.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *lab = [[UILabel alloc] init];
    lab.frame = CGRectMake(10, 100, self.view.bounds.size.width-20, 30);
    lab.backgroundColor = [UIColor redColor];
    lab.textColor = [UIColor whiteColor];
    lab.text = @"我是控制器0";
    [self.view addSubview:lab];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
