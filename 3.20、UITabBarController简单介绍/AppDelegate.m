//
//  AppDelegate.m
//  3.20、UITabBarController简单介绍
//
//  Created by 李坤 on 16/12/13.
//
//

#import "AppDelegate.h"
#import "TabBarController1.h"
#import "ViewController.h"
#import "ViewController1.h"
#import "ViewController2.h"
#import "ViewController3.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    //1、初始化UITabBarController
    UITabBarController *controller = [[TabBarController1 alloc] init];
    //设置控制器为window的根控制器
    self.window.rootViewController = controller;
    
    //下面将创建4个控制器，每种控制器的创建方式不同，注意了
    [controller addChildViewController:[self m1]];
    [controller addChildViewController:[self m2]];
    [controller addChildViewController:[self m3]];
    [controller addChildViewController:[self m4]];
    
    [self.window makeKeyAndVisible];
    
    
    
    return YES;
}

//通过Storeboard创建
- (UIViewController *)m1{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [sb instantiateInitialViewController];
    controller.view.backgroundColor = [UIColor colorWithRed:1.0 green:0 blue:0 alpha:1];
//    controller.tabBarItem.image = [UIImage imageNamed:@"1"];
    controller.tabBarItem.title = @"tab0";
    return controller;
}

//通过代码创建
- (UIViewController *)m2{
    ViewController1 *controller = [[ViewController1 alloc] init];
    controller.view.backgroundColor = [UIColor colorWithRed:0 green:1.0 blue:0 alpha:1];
//    controller.tabBarItem.image = [UIImage imageNamed:@"2"];
    controller.tabBarItem.title = @"tab1";
    return controller;
}

//通过xib创建,指定xib名称
- (UIViewController *)m3{
    ViewController2 *controller = [[ViewController2 alloc] initWithNibName:@"ViewController2" bundle:nil];
    controller.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:1];
//    controller.tabBarItem.image = [UIImage imageNamed:@"3"];
    controller.tabBarItem.title = @"tab2";
    return controller;
}

//通过代码创建
- (UIViewController *)m4{
    //注意系统中有一个和ViewControl3同名的xib文件,文件名称为ViewController3.xib，创建ViewController3会自动查找同名的xib进行创建
    ViewController3 *controller = [[ViewController3 alloc] init];
    controller.view.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:0 alpha:1];
//    controller.tabBarItem.image = [UIImage imageNamed:@"4"];
    
    controller.tabBarItem.title = @"tab3";
    controller.tabBarItem.titlePositionAdjustment=UIOffsetMake(10, 0);
    
    return controller;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
