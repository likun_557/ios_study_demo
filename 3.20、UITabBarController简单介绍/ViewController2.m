//
//  ViewController2.m
//  iOS
//
//  Created by 李坤 on 16/12/13.
//
//

#import "ViewController2.h"

@interface ViewController2 ()

@end

@implementation ViewController2

- (void)loadView{
    NSLog(@"----start %s",__func__);
    [super loadView];
    NSLog(@"----end %s",__func__);
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"----%s",__func__);
}

- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"----%s",__func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"----%s",__func__);
    self.view.frame = [UIScreen mainScreen].bounds;
    NSLog(@"%@",NSStringFromCGRect(self.view.frame));
    UILabel *lab = [[UILabel alloc] init];
    lab.frame = CGRectMake(10, 100, self.view.bounds.size.width-20, 30);
    lab.backgroundColor = [UIColor redColor];
    lab.textColor = [UIColor whiteColor];
    lab.text = @"我是控制器2";
    [self.view addSubview:lab];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
