//
//  YYtgcell.m
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import "YYtgcell.h"

@interface YYtgcell ()
@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UILabel *titlelab;
@property (strong, nonatomic) IBOutlet UILabel *pricelab;
@property (strong, nonatomic) IBOutlet UILabel *buycountlab;

@end

@implementation YYtgcell

- (void)setYytg:(YYtgModel *)yytg{
    _yytg = yytg;
    self.img.image = [UIImage imageNamed:yytg.icon];
    self.titlelab.text = yytg.title;
    self.pricelab.text = yytg.price;
    self.buycountlab.text = yytg.buyCount;
}

@end
