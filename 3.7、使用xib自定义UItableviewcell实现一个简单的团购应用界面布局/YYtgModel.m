//
//  YYtg.m
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import "YYtgModel.h"

@implementation YYtgModel

- (instancetype)initWithByCount:(NSString *)buyCount andIcon:(NSString *)icon andPrice:(NSString *)price andTitle:(NSString *)title{
    if(self = [super init]){
        self.buyCount = buyCount;
        self.icon = icon;
        self.price = price;
        self.title = title;
    }
    return self;
}

- (instancetype)initWithDict:(NSMutableDictionary *)dict{
    if(self = [super init]){
        [self setValuesForKeysWithDictionary:dict];
    }
    return  self;
}

+ (instancetype)tgWithByCount:(NSString *)buyCount andIcon:(NSString *)icon andPrice:(NSString *)price andTitle:(NSString *)title{
    return [[self alloc] initWithByCount:buyCount andIcon:icon andPrice:price andTitle:title];
}

+ (instancetype)tgWithDict:(NSDictionary *)dict{
    return [[self alloc] initWithDict:dict];
}

@end
