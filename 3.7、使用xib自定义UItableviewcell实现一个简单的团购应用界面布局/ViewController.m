//
//  ViewController.m
//  3.7、使用xib自定义UItableviewcell实现一个简单的团购应用界面布局
//
//  Created by 李坤 on 16/12/7.
//
//

#import "ViewController.h"
#import "YYtgModel.h"
#import "YYtgcell.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,weak)UITableView *tableview;

@property(nonatomic,strong)NSMutableArray *tgs;

@end

@implementation ViewController

- (NSMutableArray *)tgs{
    if(!_tgs){
        NSLog(@"%@",NSFontAttributeName);
        _tgs = [NSMutableArray array];
        NSString *path = [[NSBundle mainBundle] pathForResource:@"tgs" ofType:@"plist"];
        NSArray<NSDictionary *> *arr = [[NSArray alloc] initWithContentsOfFile:path];
        for(NSDictionary *dict in arr){
            [_tgs addObject:[YYtgModel tgWithDict:dict]];
        }
    }
    return _tgs;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.tgs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"tg";
    YYtgcell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell){
        cell = [[[NSBundle mainBundle] loadNibNamed:@"tgcell" owner:nil options:nil] firstObject];
        NSLog(@"--%lu",cell.contentView.subviews.count);
        NSLog(@"--%@",cell.subviews);
        NSLog(@"%@",[[NSBundle mainBundle] loadNibNamed:@"tgcell" owner:nil options:nil]);
        cell.yytg = self.tgs[indexPath.row];
        NSLog(@"创建了一个cell");
    }
    return cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UITableView *tv = [[UITableView alloc] init];
    NSUInteger marginTop = 30;
    tv.frame = CGRectMake(0, marginTop, self.view.bounds.size.width, self.view.bounds.size.height-marginTop);
    tv.delegate = self;
    tv.dataSource = self;
    tv.rowHeight = 100.f;
    [self.view addSubview:tv];
    
    self.tableview = tv;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
