//
//  YYtg.h
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import <Foundation/Foundation.h>

@interface YYtgModel : NSObject
@property(nonatomic,copy)NSString *buyCount;
@property(nonatomic,copy)NSString *icon;
@property(nonatomic,copy)NSString *price;
@property(nonatomic,copy)NSString *title;

- (instancetype)initWithByCount:(NSString *)buyCount andIcon:(NSString *)icon andPrice:(NSString *)price andTitle:(NSString *)title;

- (instancetype)initWithDict:(NSMutableDictionary *)dict;

+ (instancetype)tgWithByCount:(NSString *)buyCount andIcon:(NSString *)icon andPrice:(NSString *)price andTitle:(NSString *)title;

+ (instancetype)tgWithDict:(NSDictionary *)dict;

@end
