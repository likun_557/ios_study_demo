//
//  main.m
//  3.7、使用xib自定义UItableviewcell实现一个简单的团购应用界面布局
//
//  Created by 李坤 on 16/12/7.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
