//
//  main.m
//  3.23、线程间通信-异步加载图片
//
//  Created by 李坤 on 16/12/14.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
