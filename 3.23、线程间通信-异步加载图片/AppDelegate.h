//
//  AppDelegate.h
//  3.23、线程间通信-异步加载图片
//
//  Created by 李坤 on 16/12/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

