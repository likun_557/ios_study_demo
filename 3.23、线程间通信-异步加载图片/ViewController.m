//
//  ViewController.m
//  3.23、线程间通信-异步加载图片
//
//  Created by 李坤 on 16/12/14.
//
//

#import "ViewController.h"

@interface ViewController ()
@property(nonatomic,weak)UIImageView *imgview;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self m1];
    
}

- (void)m1{
    UIImageView *imgv = [[UIImageView alloc] init];
    CGRect stbarframe = [UIApplication sharedApplication].statusBarFrame;
    imgv.frame = CGRectMake(0, stbarframe.size.height, self.view.bounds.size.width, self.view.bounds.size.height - stbarframe.size.height);
    [self.view addSubview:imgv];
    //imgv.layer.borderWidth = 2;
    //imgv.layer.borderColor = [[UIColor redColor] CGColor];
    self.imgview.backgroundColor = [UIColor grayColor];
    self.imgview = imgv;
    [self downLoadImg:@"http://pic.ffpic.com/files/2014/0222/0221drxjiphonejxbz1.jpg" ];
}

- (void)downLoadImg:(NSString *)urls{
    [self performSelectorInBackground:@selector(exeDownLoadImg:) withObject:urls];
}

- (void)exeDownLoadImg:(NSObject *)urls{
    NSLog(@"加载图片start--%@",urls);
    //1.根据url下载图片(加载网络图片)
    NSURL *url = [NSURL URLWithString:[urls description]];
    
    //把图片转换为二进制数据(此步比较耗时)
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    //把数据转换为图片
    UIImage *image = [UIImage imageWithData:data];
    
    NSLog(@"加载图片--end,%@",NSStringFromCGSize(image.size));
    [self performSelectorOnMainThread:@selector(downLoadImgCall:) withObject:image waitUntilDone:NO];
    
    
}
- (void)downLoadImgCall:(UIImage *)img{
    self.imgview.image = img;
    self.imgview.contentMode = UIViewContentModeScaleAspectFill;
    self.imgview.clipsToBounds = YES;
    NSLog(@"ok");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
