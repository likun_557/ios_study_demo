//
//  ViewController.m
//  3.25、NSOperation
//
//  Created by 李坤 on 16/12/15.
//
//

#import "ViewController.h"

@interface ViewController ()
@property(nonatomic,strong)NSOperationQueue *optque;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self t6];

    
}


/*
 监听操作完成
 */

- (void)t6{
    NSLog(@"------%@-----%s------star",[NSThread currentThread],__func__);
    NSInvocationOperation *opt1 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(test1:) object:@"opt1-block1"];
    [opt1 setCompletionBlock:^{
        NSLog(@"opt1-block1执行完毕,%@",[NSThread currentThread]);
    }];
    NSInvocationOperation *opt2 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(test1:) object:@"opt2-block1"];
    [opt2 setCompletionBlock:^{
        NSLog(@"opt2执行完毕,%@",[NSThread currentThread]);
    }];
    NSBlockOperation *opt3 = [NSBlockOperation blockOperationWithBlock:^{
        [self test1:@"opt3-block1"];
    }];
    [opt3 addExecutionBlock:^{
        [self test1:@"opt3-block2"];
    }];
    [opt3 setCompletionBlock:^{
        NSLog(@"opt3执行完毕,%@",[NSThread currentThread]);
    }];
    [opt1 addDependency:opt2];
    [opt1 addDependency:opt3];
    [opt2 addDependency:opt3];
    self.optque = [[NSOperationQueue alloc] init];
    //    self.optque.maxConcurrentOperationCount =2;
    [self.optque addOperations:@[opt1,opt2,opt3] waitUntilFinished:YES];//此处为YES表示下面需要等待上面的任务执行完毕继续执行，NO表示不需要等待
    NSLog(@"------%@-----%s------end",[NSThread currentThread],__func__);
}

/*
 操作依赖，比如让A操作完毕之后才能执行B操作.
 注意：不能够循环依赖.
 */
- (void)t5{
    NSLog(@"------%@-----%s------star",[NSThread currentThread],__func__);
    NSInvocationOperation *opt1 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(test1:) object:@"opt1-block1"];
    
    NSInvocationOperation *opt2 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(test1:) object:@"opt2-block1"];
    
    NSBlockOperation *opt3 = [NSBlockOperation blockOperationWithBlock:^{
        [self test1:@"opt3-block1"];
    }];
    [opt3 addExecutionBlock:^{
        [self test1:@"opt3-block2"];
    }];
    
    [opt1 addDependency:opt2];
    [opt1 addDependency:opt3];
    [opt2 addDependency:opt3];
    self.optque = [[NSOperationQueue alloc] init];
//    self.optque.maxConcurrentOperationCount =2;
    [self.optque addOperations:@[opt1,opt2,opt3] waitUntilFinished:YES];//此处为YES表示下面需要等待上面的任务执行完毕继续执行，NO表示不需要等待
    NSLog(@"------%@-----%s------end",[NSThread currentThread],__func__);
}

/*
 暂停队列，恢复队列
 正在执行的任务不能暂停，暂停只对还未执行的任务起效
 */
- (void)t4{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIButton *btn1 = [[UIButton alloc] init];
        btn1.frame = CGRectMake(50, 50, self.view.bounds.size.width-200, 30);
        [btn1 setTitle:@"暂停队列" forState:UIControlStateNormal];
        btn1.backgroundColor = [UIColor redColor];
        btn1.titleLabel.textColor = [UIColor whiteColor];
        btn1.titleLabel.textAlignment = NSTextAlignmentCenter;
        btn1.tag = 1;
        [btn1 addTarget:self action:@selector(btnClick:andEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn1];
        
        UIButton *btn2 = [[UIButton alloc] init];
        btn2.frame = CGRectMake(50, CGRectGetMaxY(btn1.frame)+20, self.view.bounds.size.width-200, 30);
        [btn2 setTitle:@"恢复队列" forState:UIControlStateNormal];
        btn2.backgroundColor = [UIColor redColor];
        btn2.titleLabel.textColor = [UIColor whiteColor];
        btn2.titleLabel.textAlignment = NSTextAlignmentCenter;
        btn2.tag = 2;
        [btn2 addTarget:self action:@selector(btnClick:andEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn2];
    });
    

    NSLog(@"------%@-----%s------star",[NSThread currentThread],__func__);
    NSInvocationOperation *opt1 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(test1:) object:@"opt1-block1"];
    
    NSInvocationOperation *opt2 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(test1:) object:@"opt2-block1"];
    
    NSBlockOperation *opt3 = [NSBlockOperation blockOperationWithBlock:^{
        [self test1:@"opt3-block1"];
    }];
    [opt3 addExecutionBlock:^{
        [self test1:@"opt3-block2"];
    }];
    self.optque = [[NSOperationQueue alloc] init];
    self.optque.maxConcurrentOperationCount =1;
    [self.optque addOperations:@[opt1,opt2,opt3] waitUntilFinished:YES];//此处为YES表示下面需要等待上面的任务执行完毕继续执行，NO表示不需要等待
    NSLog(@"------%@-----%s------end",[NSThread currentThread],__func__);
    
}

- (void)btnClick:(UIButton *)btn andEvent:(UIEvent *)event{
    if(btn.tag==1 && !self.optque.suspended){
        [self.optque setSuspended:YES];
        NSLog(@"已暂停");
    }else if(btn.tag==2 && self.optque.suspended){
        self.optque.suspended = NO;
        NSLog(@"已恢复");
    }
}

- (void)t3{
    //创建NSBlockOperation操作对象
    NSBlockOperation *operation=[NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"NSBlockOperation------%@",[NSThread currentThread]);
    }];
    
    //添加操作
    [operation addExecutionBlock:^{
        NSLog(@"NSBlockOperation1------%@",[NSThread currentThread]);
    }];
    
    [operation addExecutionBlock:^{
        NSLog(@"NSBlockOperation2------%@",[NSThread currentThread]);
    }];
    
    //开启执行操作
    [operation start];
}

//使用NSBlockOperation，当NSBlockOperation封装的任务书>1的时候，系统会自动异步执行其他的任务.
- (void)t2{
    NSLog(@"------%@-----%s------star",[NSThread currentThread],__func__);
    NSBlockOperation *op1 = [NSBlockOperation blockOperationWithBlock:^{
        [self test1:@"block1"];
    }];
    [op1 addExecutionBlock:^{
        [self test1:@"block2"];
    }];
    [op1 start];
    NSLog(@"------%@-----%s------end",[NSThread currentThread],__func__);
}

//使用NSInvocationOperation
- (void)t1{
    NSLog(@"------%@-----%s------star",[NSThread currentThread],__func__);
    NSOperation *op1 = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(test1:) object:@"NSInvocationOperation"];
    [op1 start];
    NSLog(@"------%@-----%s------end",[NSThread currentThread],__func__);
}

- (void)test1:(NSString *)tag{
    NSLog(@"------%@----%@------start",[NSThread currentThread],tag);
    int len = [tag isEqualToString:@"block2"]?10:5;
    for (int i = 0; i < len; i++) {
        [NSThread sleepForTimeInterval:0.4*i];
        NSLog(@"------%@----%@------%i",[NSThread currentThread],tag,i);
    }
    NSLog(@"------%@----%@------end",[NSThread currentThread],tag);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
