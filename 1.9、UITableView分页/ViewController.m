//
//  ViewController.m
//  1.9、UITableView分页
//
//  Created by 李坤 on 16/12/1.
//
//

#import "ViewController.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>

//tableview对象
@property(nonatomic,strong)UITableView *tableView;
//数据源
@property(nonatomic,strong)NSMutableArray *dataSource;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%s",__func__);
    _dataSource = [[NSMutableArray alloc] init];
    for (int i = 0; i < 10; i++) {
        [_dataSource addObject:[NSString stringWithFormat:@"UITableView分页%d",i]];
    }
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"%s",__func__);
    return self.dataSource.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%s",__func__);
    NSString *reusedStr = @"demo";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusedStr];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusedStr];
    }
    
    if(indexPath.row == _dataSource.count){
        //定制最后一行的cell
        cell.textLabel.text = @"加载更多";
    }else{
        //定制普通的cell
        cell.textLabel.text = self.dataSource[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%s",__func__);
    if(indexPath.row == self.dataSource.count){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelector:@selector(loadMore) withObject:nil];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        });
        return;
    }
}

- (void)loadMore{
    //添加数据
    for (int i = 0; i < 10; i++) {
        [self.dataSource addObject:[NSString stringWithFormat:@"新%d",i]];
    }
    //重新加载tableview
    [self.tableView reloadData];
}



@end
