//
//  ViewController.m
//  3.2、UIImageView
//
//  Created by 李坤 on 16/11/29.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self createUIImageView];
    [self initBackgroundImage];
    [self imageview1];
    [self imageview2];
}

/**
 *  创建背景图片并添加毛玻璃效果
 */
- (void)initBackgroundImage{
    //1、创建imageview作为背景
    UIImageView *bgImageView = [[UIImageView alloc] init];
    
    //2、设置frame
    bgImageView.frame = self.view.bounds;
    
    //3、设置背景图片
    bgImageView.image = [UIImage imageNamed:@"3"];
    
    //4、设置内容模式
    bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    //5、添加毛玻璃效果
    //5.1、创建UIToolbar对象
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    //5.2、设置frame
    toolbar.frame = bgImageView.bounds;
    //5.3、设置toolbar的样式
    toolbar.barStyle=UIBarStyleBlack;
    //将toolbar添加到bgImageView中
    [bgImageView addSubview:toolbar];
    
    //将bgImageView添加到控制器的view
    [self.view addSubview:bgImageView];
}


/**
 *  创建UIImageView
 */
- (void)createUIImageView{
    //1.创建UIImageView对象
    UIImageView *imageView = [[UIImageView alloc] init];
    //2.设置frame
    imageView.frame = CGRectMake(0, 0, 275, 300);
    //3.设置背景颜色
    imageView.backgroundColor = [UIColor yellowColor];
    //4.设置图片
    imageView.image = [UIImage imageNamed:@"3"];
    //5.设置图片模式
    /*
     //带sacle：比例--图片之后可能会被缩放
     UIViewContentModeScaleToFill,//压缩或者拉伸图片，让图片填充整个空间
     UIViewContentModeScaleAspectFit,//宽高比例不变，图片可以被拉伸，也可以被压缩，但是要保持宽高比，fit：适应一部分填充
     UIViewContentModeScaleAspectFill,//宽高比例不变，图片可以被拉伸，也可以被压缩，但是要保持宽高比，fill：填充
     
    
     UIViewContentModeRedraw,              // redraw on bounds change (calls -setNeedsDisplay)
     
     //图片不会被拉伸和压缩
     UIViewContentModeCenter,              // contents remain same size. positioned adjusted.
     UIViewContentModeTop,
     UIViewContentModeBottom,
     UIViewContentModeLeft,
     UIViewContentModeRight,
     UIViewContentModeTopLeft,
     UIViewContentModeTopRight,
     UIViewContentModeBottomLeft,
     UIViewContentModeBottomRight,
     */
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = YES;
    //5.将imageView添加到当前视图中
    [self.view addSubview:imageView];
}

- (void)imageview1{
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *imgFilePath = [bundle pathForResource:@"2.jpg" ofType:nil];
    NSLog(@"%@",imgFilePath);
    UIImage *image = [UIImage imageWithContentsOfFile:imgFilePath];
    UIImageView *img1 = [[UIImageView alloc] initWithImage:image];
    [self.view addSubview:img1];
}

- (void)imageview2{
    UIImage *image = [UIImage imageNamed:@"images/spring.jpg"];
    UIImageView *img1 = [[UIImageView alloc] initWithImage:image];
    [self.view addSubview:img1];
}
@end
