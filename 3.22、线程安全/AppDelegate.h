//
//  AppDelegate.h
//  3.22、线程安全
//
//  Created by 李坤 on 16/12/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

