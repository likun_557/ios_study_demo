//
//  AppDelegate.h
//  3.18、UIWindow简单介绍
//
//  Created by 李坤 on 16/12/12.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

