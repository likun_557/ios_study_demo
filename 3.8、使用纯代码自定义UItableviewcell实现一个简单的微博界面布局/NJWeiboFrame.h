//
//  NJWeiboFrame.h
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class NJWeibo;
@interface NJWeiboFrame : NSObject
/**
 *  头像frame
 */
@property(nonatomic,assign)CGRect iconF;

/**
 *  昵称的frame
 */
@property(nonatomic,assign)CGRect nameF;
/**
 *  vip的frame
 */
@property(nonatomic,assign)CGRect vipF;
/**
 *  正文的frame
 */
@property(nonatomic,assign)CGRect introF;
/**
 *  配图的frame
 */
@property(nonatomic,assign)CGRect pictureF;
/**
 *  行高
 */
@property(nonatomic,assign)CGFloat cellHeight;

/**
 *  模型数据
 */
@property(nonatomic,strong)NJWeibo *weibo;
@end
