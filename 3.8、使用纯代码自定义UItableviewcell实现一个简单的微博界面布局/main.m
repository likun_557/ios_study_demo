//
//  main.m
//  3.8、使用纯代码自定义UItableviewcell实现一个简单的微博界面布局
//
//  Created by 李坤 on 16/12/7.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
