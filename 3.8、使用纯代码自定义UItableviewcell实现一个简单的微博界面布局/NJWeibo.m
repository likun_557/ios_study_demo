//
//  NJWeibo.m
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import "NJWeibo.h"

@implementation NJWeibo

- (instancetype)initWithDict:(NSDictionary *)dict{
    if(self = [super init]){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)weiboWithDict:(NSDictionary *)dict{
    return [[self alloc] initWithDict:dict];
}

@end
