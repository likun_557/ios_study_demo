//
//  NJWeiboCell.h
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import <UIKit/UIKit.h>
@class NJWeiboFrame;

@interface NJWeiboCell : UITableViewCell
@property(nonatomic,strong)NJWeiboFrame *weiboFrame;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
