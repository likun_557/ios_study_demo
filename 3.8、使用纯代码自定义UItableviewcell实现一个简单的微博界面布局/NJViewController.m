//
//  NJViewController.m
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import "NJViewController.h"
#import "NJWeibo.h"
#import "NJWeiboCell.h"
#import "NJWeiboFrame.h"

@interface NJViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSArray *statusFrames;
@end

@implementation NJViewController

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.statusFrames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NJWeiboCell *cell = [NJWeiboCell cellWithTableView:tableView];
    //设置数据
    cell.weiboFrame = self.statusFrames[indexPath.row];
    //返回
    return cell;
}

#pragma mark - 懒加载
- (NSArray *)statusFrames{
    if(_statusFrames == nil){
        NSString *fullPath = [[NSBundle mainBundle] pathForResource:@"statuses.plist" ofType:nil];
        NSArray *dictArray = [NSArray arrayWithContentsOfFile:fullPath];
        NSMutableArray *models = [NSMutableArray arrayWithCapacity:dictArray.count];
        for (NSDictionary *dict in dictArray) {
            //创建模型
            NJWeibo *weibo = [NJWeibo weiboWithDict:dict];
            //根据模型数据创建frame模型
            NJWeiboFrame *wbF = [[NJWeiboFrame alloc] init];
            wbF.weibo = weibo;
            [models addObject:wbF];
        }
        self.statusFrames = [models copy];
    }
    return _statusFrames;
}

#pragma mark - 代理方法
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //取出对应行的frame模型
    NJWeiboFrame *wbF = self.statusFrames[indexPath.row];
    NSLog(@"height = %f",wbF.cellHeight);
    return wbF.cellHeight;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    UITableView *tv = [[UITableView alloc] initWithFrame:self.view.bounds];
    tv.bounds = CGRectMake(0, 20, self.view.bounds.size.width, self.view.bounds.size.height - 20);
    tv.delegate = self;
    tv.dataSource = self;
    [self.view addSubview:tv];
}

@end
