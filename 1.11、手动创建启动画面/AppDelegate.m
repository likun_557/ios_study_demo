//
//  AppDelegate.m
//  1.11、手动创建启动画面
//
//  Created by 李坤 on 16/12/3.
//
//

#import "AppDelegate.h"
#import "ViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self showMainWindow];
    // Override point for customization after application launch.
    return YES;
}

- (void)showMainWindow{
    //设置window属性
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen]bounds]];
    //设置window的背景
    self.window.backgroundColor = [UIColor whiteColor];
    //初始化ViewController
    ViewController *mainController = [[ViewController alloc] init];
    //设置自定义控制器的大小和window相同，位置为(0,0)
    mainController.view.frame = self.window.bounds;
    //设置此控制器为window的根控制器
    self.window.rootViewController = mainController;
    
    //设置window为应用程序主窗口并设置为空间
    [self.window makeKeyAndVisible];
    NSLog(@"%@",self.window);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
