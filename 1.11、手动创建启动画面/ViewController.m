//
//  ViewController.m
//  1.11、手动创建启动画面
//
//  Created by 李坤 on 16/12/3.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"哈哈哈");
    NSLog(@"%@",NSStringFromCGRect(self.view.bounds));
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(100, 100, 200, 30)];
    label.text = @"hello world";
    label.backgroundColor = [UIColor grayColor];
    [self.view addSubview:label];
    
    //添加按钮
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(100, 150, 100, 30)];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [btn setTitle:@"点我" forState:UIControlStateNormal];
    //添加点击事件
    /*
     - (void)addTarget:(nullable id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;
     参数1：事件监听器
     参数2：监听器中的方法
     参数3：事件类型
     */
    [btn addTarget:self action:@selector(clickWithBtn:forEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)clickWithBtn:(UIButton *)sender forEvent:(UIEvent *)event{
    NSLog(@"%s",__func__);
    NSLog(@"%@,%@",sender,event);
    NSLog(@"%@",NSStringFromClass([event class]));
    NSLog(@"%lu",event.type);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
