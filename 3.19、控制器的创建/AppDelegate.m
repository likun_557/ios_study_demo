//
//  AppDelegate.m
//  3.19、控制器的创建
//
//  Created by 李坤 on 16/12/12.
//
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "ViewController1.h"
#import "ViewController2.h"
#import "ViewController3.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    //创建viewcontroll的几种方式
    [self m4];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

/**
 *  方式1：通过uistoreboard创建,storeboard中只有一控制器的情况
 */
- (void)m1{
    //加载storyboard,（注意：这里仅仅是加载名称为test的storyboard,并不会创建storyboard中的控制器和控件）
    UIStoryboard *storeboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //下面这个方法代表着创建storyboard中箭头指向的控制器（初始控制器）
    ViewController *controller = [storeboard instantiateInitialViewController];
    NSLog(@"哈哈哈%i",[controller isViewLoaded]);
    NSLog(@"ViewController======$%@",controller.view);
    NSLog(@"哈哈哈%i",[controller isViewLoaded]);
    self.window.rootViewController = controller;
}

/**
 *  方式2：通过uistoreboard创建，storeboard有多个控制器的情况
 */
- (void)m2{
    UIStoryboard *storeboard = [UIStoryboard storyboardWithName:@"ViewController3" bundle:nil];
    ViewController3 *controller = [storeboard instantiateViewControllerWithIdentifier:@"controller1"];
    NSLog(@"$$$$$$$%@",controller.view);
    self.window.rootViewController = controller;
}

/**
 *  方式3：代码创建
 */
- (void)m3{
    ViewController1 *controller = [[ViewController1 alloc] init];
    NSLog(@"哈哈哈");
    NSLog(@"ViewController1======$%@",controller.view);
    self.window.rootViewController = controller;
}

/**
 *  方式4：通过xib创建
 */
- (void)m4{
    ViewController2 *controller = [[ViewController2 alloc] initWithNibName:@"ViewController2" bundle:nil];
    self.window.rootViewController = controller;
}

- (void)m5{
    ViewController2 *controller = [[ViewController2 alloc] init];
    self.window.rootViewController = controller;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
