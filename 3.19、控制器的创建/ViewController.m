//
//  ViewController.m
//  3.19、控制器的创建
//
//  Created by 李坤 on 16/12/12.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
- (void)loadView{
    [super loadView];
    NSLog(@"%s",__func__);
}
- (void)viewDidLoad{
    NSLog(@"%s,%@",__func__,self.view);
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
