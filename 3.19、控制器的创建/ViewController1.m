//
//  ViewController1.m
//  iOS
//
//  Created by 李坤 on 16/12/12.
//
//

#import "ViewController1.h"

@interface ViewController1 ()

@end

@implementation ViewController1

- (void)loadView{
    [super loadView];
    NSLog(@"%s",__func__);
}
- (void)viewDidLoad{
    NSLog(@"%s,%@",__func__,self.view);
    [super viewDidLoad];
    NSLog(@"%s,%@",__func__,self.view);
    UILabel *lbl = [[UILabel alloc] init];
    lbl.numberOfLines = 0;
    lbl.frame = CGRectMake(10, 100, self.view.bounds.size.width - 20, 100);
    lbl.text = [NSString stringWithUTF8String:__func__];
    [self.view addSubview:lbl];
}

@end
