//
//  ViewController.m
//  1.8、UITableView无分组示例
//
//  Created by 李坤 on 16/12/1.
//
//

#import "ViewController.h"
#import "Blog.h"

@interface ViewController ()<UITableViewDataSource>

@property(nonatomic,weak)UITableView *tableView;
@property(nonatomic,strong)NSArray<Blog *> *blogs;

@end

@implementation ViewController

/**
 *  返回多少组
 *
 *  @param tableView tableview对象
 *
 *  @return 返回当前tableview有多少组
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

/**
 *  返回行数
 *
 *  @param tableView tableview对象
 *  @param section   组下标
 *
 *  @return 组中的行数
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.blogs.count;
}

/**
 *  每行显示的内容
 *
 *  @param tableView tableview对象
 *  @param indexPath cell信息
 *
 *  @return 行内容
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    if((indexPath.row+1)%2 == 0){
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
    }
    cell.textLabel.text = self.blogs[indexPath.row].title;
    return cell;
}

#pragma mark 在当前视图中创建tableview
/**
 *  在当前视图中创建tableview
 */
- (void)createTableView{
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.frame = CGRectMake(0, 20, self.view.bounds.size.width, self.view.bounds.size.height);
    tableView.dataSource = self;
    [self.view addSubview:tableView];
}

/**
 *  获取数据
 *
 *  @return 返回数据源数据
 */
- (NSArray<Blog *> *)blogs{
    if(self->_blogs==nil){
        NSMutableArray *blogs = [NSMutableArray array];
        for (int i = 0; i < 100; i++) {
            [blogs addObject:[Blog blogWithTitle:[NSString stringWithFormat:@"%@-%i",@"iOS开发UI篇—UITableview控件基本使用",i]]];
        }
        _blogs = blogs;
    }
    return _blogs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
