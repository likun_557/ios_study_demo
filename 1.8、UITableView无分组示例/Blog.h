//
//  Blog.h
//  iOS
//
//  Created by 李坤 on 16/12/1.
//
//

#import <Foundation/Foundation.h>

@interface Blog : NSObject

@property(nonatomic,copy)NSString *title;

- (instancetype)initWithTitle:(NSString *)title;

+ (instancetype)blogWithTitle:(NSString *)title;

@end
