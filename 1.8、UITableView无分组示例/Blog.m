//
//  Blog.m
//  iOS
//
//  Created by 李坤 on 16/12/1.
//
//

#import "Blog.h"

@implementation Blog

- (instancetype)initWithTitle:(NSString *)title{
    if(self=[super init]){
        self.title = title;
    }
    return self;
}

+ (instancetype)blogWithTitle:(NSString *)title{
    return [[self alloc] initWithTitle:title];
}

@end
