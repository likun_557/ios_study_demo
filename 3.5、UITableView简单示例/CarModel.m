//
//  CarModel.m
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import "CarModel.h"

@implementation CarModel

- (instancetype)initWithName:(NSString *)name{
    if(self = [super init]){
        self.name = name;
    }
    return self;
}

+ (instancetype)carWithName:(NSString *)name{
    return [[self alloc] initWithName:name];
}

@end
