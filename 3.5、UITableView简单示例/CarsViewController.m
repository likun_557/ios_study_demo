/*
 UITableView使用过程：
 1、实现UITableViewDataSource协议，设置数据源，遵守协议
 2、返回组数
 3、返回行中的列数
 4、返回每组每行对应的数据
    4.1、去缓存中取cell
    4.2、若没有，创建cell，并盖章
    4.3、设置cell的数据
    4.4、返回cell
 */

#import "CarsViewController.h"
#import "CarGroupModel.h"
#import "CarModel.h"
@interface CarsViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSMutableArray<CarGroupModel *> *carGroupArray;
@property(nonatomic,weak)UITableView *tableView;
@end

@implementation CarsViewController

//懒加载数据
- (NSMutableArray<CarGroupModel *> *)carGroupArray{
    if(_carGroupArray==nil){
        self->_carGroupArray = [NSMutableArray array];
        
        NSMutableArray *cars = [NSMutableArray array];
        
        for (int i=0; i<5; i++) {
            NSString *index = [NSString stringWithFormat:@"%i",i];
            [cars removeAllObjects];
            [cars addObject:[CarModel carWithName:@"奥迪"]];
            [cars addObject:[CarModel carWithName:@"阿斯顿.马丁"]];
            [cars addObject:[CarModel carWithName:@"阿尔法.罗密欧"]];
            [cars addObject:[CarModel carWithName:@"阿尔法"]];
            [cars addObject:[CarModel carWithName:@"奥克斯"]];
            [self->_carGroupArray addObject:[CarGroupModel carGroupWithName:[NSString stringWithFormat:@"A%@",index] andCars:cars]];
            
            [cars removeAllObjects];
            [cars addObject:[CarModel carWithName:@"本田"]];
            [cars addObject:[CarModel carWithName:@"奔驰"]];
            [cars addObject:[CarModel carWithName:@"别克"]];
            [cars addObject:[CarModel carWithName:@"宝马"]];
            [cars addObject:[CarModel carWithName:@"宝骏"]];
            [cars addObject:[CarModel carWithName:@"标志"]];
            [cars addObject:[CarModel carWithName:@"比亚迪"]];
            [cars addObject:[CarModel carWithName:@"保时捷"]];
            [cars addObject:[CarModel carWithName:@"北汽幻速"]];
            [cars addObject:[CarModel carWithName:@"奔腾"]];
            [cars addObject:[CarModel carWithName:@"宾利"]];
            
            [self->_carGroupArray addObject:[CarGroupModel carGroupWithName:[NSString stringWithFormat:@"B%@",index] andCars:cars]];
            
            [cars removeAllObjects];
            [cars addObject:[CarModel carWithName:@"长安轿车"]];
            [cars addObject:[CarModel carWithName:@"长安商用"]];
            [cars addObject:[CarModel carWithName:@"长城"]];
            [cars addObject:[CarModel carWithName:@"昌河"]];
            [cars addObject:[CarModel carWithName:@"长城"]];
            [cars addObject:[CarModel carWithName:@"长城华冠"]];
            [cars addObject:[CarModel carWithName:@"成功"]];
            
            [self->_carGroupArray addObject:[CarGroupModel carGroupWithName:[NSString stringWithFormat:@"C%@",index] andCars:cars]];
            
            [cars removeAllObjects];
            [cars addObject:[CarModel carWithName:@"大众"]];
            [cars addObject:[CarModel carWithName:@"东风风行"]];
            [cars addObject:[CarModel carWithName:@"东南"]];
            [cars addObject:[CarModel carWithName:@"东风风神"]];
            [cars addObject:[CarModel carWithName:@"东风风光"]];
            [cars addObject:[CarModel carWithName:@"道奇"]];
            [cars addObject:[CarModel carWithName:@"DS"]];
            
            [self->_carGroupArray addObject:[CarGroupModel carGroupWithName:[NSString stringWithFormat:@"D%@",index] andCars:cars]];
            
            [cars removeAllObjects];
            [cars addObject:[CarModel carWithName:@"丰田"]];
            [cars addObject:[CarModel carWithName:@"福特"]];
            [cars addObject:[CarModel carWithName:@"法拉利"]];
            [cars addObject:[CarModel carWithName:@"福田"]];
            [cars addObject:[CarModel carWithName:@"菲斯克"]];
            [cars addObject:[CarModel carWithName:@"飞驰商务"]];
            [cars addObject:[CarModel carWithName:@"福气启腾"]];
            
            [self->_carGroupArray addObject:[CarGroupModel carGroupWithName:[NSString stringWithFormat:@"E%@",index] andCars:cars]];
        }
    }
    return self->_carGroupArray;
}
//调用数据源下面的方法，得知总共有多少组数据
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.carGroupArray.count;
}

//调用数据源下面的方法，得知每一组有多少行数据
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.carGroupArray[section].cars.count;
}

//调用数据源下面的方法，得知每一行显示什么内容
static int i = 0;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"carviewcell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    CarGroupModel *carGroupModel = self.carGroupArray[indexPath.section];
    NSMutableArray<CarModel *> *cars = carGroupModel.cars;
    cell.textLabel.text = cars[indexPath.row].name;
    cell.textLabel.textColor = [UIColor redColor];
    cell.imageView.image = [UIImage imageNamed:@"2"];
//    cell.accessoryType = UITableViewCellAccessoryDetailButton;
//    cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
//    if(i==0){
//        NSLog(@"%@",cell.subviews[0]);
//        NSLog(@"%@",cell.contentView.subviews);
//        i++;
//    }
    return cell;
}
//
//- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"哈哈哈");
//}

//调用数据源下面的方法，得知每一组的头显示什么内容
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    NSLog(@"%s",__func__);
    return self.carGroupArray[section].name;
}

//返回右边索引数组
- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return [self.carGroupArray valueForKey:@"name"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UITableView *tableView = [[UITableView alloc] init];
    tableView.frame = CGRectMake(0, 30, self.view.bounds.size.width, self.view.bounds.size.height-30);
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorColor = [UIColor brownColor];
    tableView.rowHeight = 50.f;
    tableView.sectionIndexColor = [UIColor blueColor];
    [self.view addSubview:tableView];
    self.tableView = tableView;
}

//响应点击索引时的委托方法
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
//    NSLog(@"%s",__func__);
//    NSLog(@"titile = %@,index = %lu",title,index);
//    NSInteger result = random()%14;
//    NSLog(@"result = %lu",result);
//    return result;
    return index;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
