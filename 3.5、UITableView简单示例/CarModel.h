//
//  CarModel.h
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import <Foundation/Foundation.h>
/**
 *  汽车对象
 */
@interface CarModel : NSObject

@property(nonatomic,copy)NSString *name;

- (instancetype)initWithName:(NSString *)name;

+ (instancetype)carWithName:(NSString *)name;

@end
