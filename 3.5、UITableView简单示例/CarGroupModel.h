//
//  CarGroupModel.h
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import <Foundation/Foundation.h>
@class CarModel;
/**
 *  汽车组
 */
@interface CarGroupModel : NSObject
//组名
@property(nonatomic,copy)NSString *name;
//汽车列表
@property(nonatomic,copy)NSMutableArray<CarModel *> *cars;

- (instancetype)initWithName:(NSString *)name andCars:(NSMutableArray *)cars;

+ (instancetype)carGroupWithName:(NSString *)name andCars:(NSMutableArray *)cars;
@end
