//
//  CarGroupModel.m
//  iOS
//
//  Created by 李坤 on 16/12/7.
//
//

#import "CarGroupModel.h"

@implementation CarGroupModel

- (instancetype)initWithName:(NSString *)name andCars:(NSMutableArray *)cars{
    if(self = [super init]){
        self.name = name;
        self.cars = cars;
    }
    return self;
}

+ (instancetype)carGroupWithName:(NSString *)name andCars:(NSMutableArray *)cars{
    return [[self alloc] initWithName:name andCars:cars];
}
@end
