//
//  main.m
//  3.12、在UItableview中实现加载更多功能
//
//  Created by 李坤 on 16/12/8.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
