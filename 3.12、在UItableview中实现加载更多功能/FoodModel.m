//
//  FoodModel.m
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import "FoodModel.h"

@implementation FoodModel

- (instancetype)initWithName:(NSString *)name andPrice:(float)price andBuyCount:(NSUInteger)buyCount andIcon:(NSString *)icon{
    if(self = [super init]){
        self.name = name;
        self.price = price;
        self.buyCount = buyCount;
        self.icon = icon;
    }
    return self;
}

+ (instancetype)foodWithName:(NSString *)name andPrice:(float)price andBuyCount:(NSUInteger)buyCount andIcon:(NSString *)icon{
    return [[self alloc] initWithName:name andPrice:price andBuyCount:buyCount andIcon:icon];
}

@end
