//
//  FoodTableViewCell.h
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import <UIKit/UIKit.h>
@class FoodModel;
@interface FoodTableViewCell : UITableViewCell

@property(nonatomic,strong)FoodModel *foodModel;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
