//
//  FoodTableViewCell.m
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import "FoodTableViewCell.h"
#import "FoodModel.h"

@interface FoodTableViewCell ()

@property(nonatomic,weak)UIImageView *iconImg;

@property(nonatomic,weak)UILabel *nameLab;

@property(nonatomic,weak)UILabel *priceLab;

@property(nonatomic,weak)UILabel *buyCountLab;


@end

@implementation FoodTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *identifier = @"foodcell";
    FoodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell==nil){
        cell = [[FoodTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
        CGFloat padding = 10;
        UIImageView *iconImg = [[UIImageView alloc] init];
        CGFloat iconImgX = padding;
        CGFloat iconImgY = padding;
        CGFloat iconImgW = 100;
        CGFloat iconImgH = 100;
        iconImg.frame = CGRectMake(iconImgX, iconImgY, iconImgW, iconImgH);
        iconImg.contentMode = UIViewContentModeScaleAspectFill;
        iconImg.clipsToBounds = YES;
        iconImg.layer.cornerRadius = 20;
        [self.contentView addSubview:iconImg];
        self.iconImg = iconImg;
        
        UILabel *nameLab = [[UILabel alloc] init];
        CGFloat nameLabX = CGRectGetMaxX(iconImg.frame)+padding;
        CGFloat nameLabY = padding;
        CGFloat nameLabW = 200;
        CGFloat nameLabH = 30;
        nameLab.frame = CGRectMake(nameLabX, nameLabY, nameLabW, nameLabH);
        nameLab.textColor = [UIColor redColor];
        [self.contentView addSubview:nameLab];
        self.nameLab = nameLab;
        
        UILabel *priceLab = [[UILabel alloc] init];
        CGFloat priceLabX = nameLabX;
        CGFloat priceLabY = CGRectGetMaxY(nameLab.frame)+padding;
        CGFloat priceLabW = 70;
        CGFloat priceLabH = 30;
        priceLab.frame = CGRectMake(priceLabX, priceLabY, priceLabW, priceLabH);
        priceLab.textColor = [UIColor orangeColor];
        [self.contentView addSubview:priceLab];
        self.priceLab = priceLab;
        
        UILabel *buyCountLab = [[UILabel alloc] init];
        CGFloat buyCountLabX = CGRectGetMaxX(priceLab.frame)+10;
        CGFloat buyCountLabY = priceLabY;
        CGFloat buyCountLabW = 120;
        CGFloat buyCountLabH = 30;
        buyCountLab.frame = CGRectMake(buyCountLabX, buyCountLabY, buyCountLabW, buyCountLabH);
        [self.contentView addSubview:buyCountLab];
        self.buyCountLab = buyCountLab;
    }
    return self;
}

- (void)setFoodModel:(FoodModel *)foodModel{
    _foodModel =foodModel;
    self.iconImg.image = [UIImage imageNamed:foodModel.icon];
    self.nameLab.text = foodModel.name;
    self.priceLab.text = [NSString stringWithFormat:@"$%.2f",foodModel.price];
    self.buyCountLab.text = [NSString stringWithFormat:@"已有%lu人购买",foodModel.buyCount];
}

@end
