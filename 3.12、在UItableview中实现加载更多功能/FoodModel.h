//
//  FoodModel.h
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import <Foundation/Foundation.h>

/**
 *  食物model
 */
@interface FoodModel : NSObject

/**
 *  名称
 */
@property(nonatomic,copy)NSString *name;

/**
 *  价格
 */
@property(nonatomic,assign)float price;

/**
 *  已销量
 */
@property(nonatomic,assign)NSUInteger buyCount;

/**
 *  图片
 */
@property(nonatomic,strong)NSString *icon;

- (instancetype)initWithName:(NSString *)name andPrice:(float)price andBuyCount:(NSUInteger)buyCount andIcon:(NSString *)icon;

+ (instancetype)foodWithName:(NSString *)name andPrice:(float)price andBuyCount:(NSUInteger)buyCount andIcon:(NSString *)icon;

@end
