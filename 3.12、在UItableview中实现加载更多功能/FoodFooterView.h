//
//  FoodFooterView.h
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import <UIKit/UIKit.h>
@class FoodFooterView;
@protocol FoodFooterViewDelegate <NSObject>

- (void)btnClick:(FoodFooterView *)foodFooterView;

@end

@interface FoodFooterView : UIView

@property(nonatomic,weak)id<FoodFooterViewDelegate> delegate;

@end
