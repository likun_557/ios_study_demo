
//
//  FoodFooterView.m
//  iOS
//
//  Created by 李坤 on 16/12/8.
//
//

#import "FoodFooterView.h"



@interface FoodFooterView ()
@property(nonatomic,weak)UIButton *btn;
@end

//底部
@implementation FoodFooterView

- (instancetype)init{
    if(self = [super init]){
        UIButton *btn = [[UIButton alloc] init];
        [btn setTitle:@"加载更多" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
        btn.backgroundColor = [UIColor redColor];
        [self addSubview:btn];
        self.btn = btn;
        NSLog(@"哈哈哈");
        
    }
    return self;
}

- (void)didMoveToSuperview{
    NSLog(@"呵呵呵");
    self.btn.frame = self.bounds;
}

- (void)btnClick{
    [self.btn setTitle:@"正在努力加载中。。。。。。" forState:UIControlStateNormal];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([self.delegate respondsToSelector:@selector(btnClick:)]){
            [self.delegate performSelector:@selector(btnClick:) withObject:self];
            [self.btn setTitle:@"加载更多" forState:UIControlStateNormal];
        }
    });
    
}

@end
