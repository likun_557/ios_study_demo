//
//  ViewController.m
//  3.12、在UItableview中实现加载更多功能
//
//  Created by 李坤 on 16/12/8.
//
//

#import "ViewController.h"
#import "FoodModel.h"
#import "FoodTableViewCell.h"
#import "FoodFooterView.h"
@interface ViewController ()<UITableViewDataSource,UITableViewDelegate,FoodFooterViewDelegate>
@property(nonatomic,weak)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray<FoodModel *> *foods;
@end

@implementation ViewController

- (NSMutableArray<FoodModel *> *)foods{
    if(_foods==nil){
        _foods = [NSMutableArray array];
        [self newFoods];
    }
    return _foods;
}

- (void)newFoods{
    static int j = 0;
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i<10; i++) {
        [arr addObject:[FoodModel foodWithName:[NSString stringWithFormat:@"红烧排骨%u",j] andPrice:10+j andBuyCount:10*j andIcon:[NSString stringWithFormat:@"%i",i%3]]];
        j++;
    }
    [_foods addObjectsFromArray:arr];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.foods.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FoodTableViewCell *cell = [FoodTableViewCell cellWithTableView:tableView];
    cell.foodModel = self.foods[indexPath.row];
    return cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.rowHeight = 120;
    
    FoodFooterView *footerFooterView = [[FoodFooterView alloc] init];
    footerFooterView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 50);
    footerFooterView.delegate = self;
    tableView.tableFooterView = footerFooterView;
    [self.view addSubview:tableView];
    self.tableView = tableView;
}

- (void)btnClick:(FoodFooterView *)foodFooterView{
    [self newFoods];
    [self.tableView reloadData];
}
- (BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
