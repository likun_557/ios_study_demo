//
//  ViewController.m
//  3.15、Date Picker和UITool Bar控件简单介绍
//
//  Created by 李坤 on 16/12/11.
//
//

#import "ViewController.h"

@interface ViewController ()

@property(nonatomic,weak)UITextField *textField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITextField *textField = [[UITextField alloc] init];
    textField.frame = CGRectMake(50, 50, self.view.bounds.size.width-100, 30);
    textField.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:textField];
    self.textField = textField;
    
    //添加一个时间选择器
    UIDatePicker *date = [[UIDatePicker alloc] init];
    //设置只显示中文
    [date setLocale:[NSLocale localeWithLocaleIdentifier:@"zh-cn"]];
    //设置只显示日期
    date.datePickerMode = UIDatePickerModeDate;
    //当光标移动到文本框的时候，召唤时间选择器
    self.textField.inputView = date;
    
    //创建工具条
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    //设置工具条的衍射
    toolbar.barTintColor = [UIColor brownColor];
    //设置工具条的frame
    toolbar.frame = CGRectMake(0, 0, self.view.bounds.size.width, 44);
    //给工具条添加按钮
    UIBarButtonItem *item0 = [[UIBarButtonItem alloc] initWithTitle:@"上一个" style:UIBarButtonItemStylePlain target:self action:@selector(click)];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"下一个" style:UIBarButtonItemStylePlain target:self action:@selector(click)];
    UIBarButtonItem *item2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *item3=[[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(click)];
    item0.tintColor = item1.tintColor = item2.tintColor = item3.tintColor = [UIColor whiteColor];
    toolbar.backgroundColor = [UIColor redColor];
    toolbar.items = @[item0,item1,item2,item3];
    //设置文本框的键盘辅助视图
    self.textField.inputAccessoryView = toolbar;
    
}

- (void)click{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
