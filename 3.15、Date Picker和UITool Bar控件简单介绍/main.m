//
//  main.m
//  3.15、Date Picker和UITool Bar控件简单介绍
//
//  Created by 李坤 on 16/12/11.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
