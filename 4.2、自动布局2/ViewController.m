//
//  ViewController.m
//  4.2、自动布局2
//
//  Created by 李坤 on 16/12/20.
//
//
#define topSpace 64
#define kMargin 20

#define kTopViewHeight 44
#define kTopViewWidth 300

#define kTextLabelWidth 200
#define kTextLabelHeight 30

#import "ViewController.h"

@interface ViewController ()
@property(nonatomic,weak)UIView *v1;
@property(nonatomic,weak)UIButton *btn;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 以Iphone4（320， 480）为基础，设置各控件的位置
    // 注意：必须所有控件都按照Iphone4（320， 480）为基础初始化一次，不然按比例缩放时会发生错误！
//    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(kMargin, topSpace, kTopViewWidth, kTopViewHeight)];
//    CGFloat textLabelTop = (topView.frame.size.width - kTextLabelWidth) / 2;
//    CGFloat textLabelWidth = (topView.frame.size.height - kTextLabelHeight) / 2;
//    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(textLabelTop, textLabelWidth, kTextLabelWidth, kTextLabelHeight)];
//    
//    // 设置文字及居中
//    [textLabel setText:@"Garvey"];
//    [textLabel setTextAlignment:NSTextAlignmentCenter];
//    
//    // 分别设置样式
//    [topView setBackgroundColor:[UIColor redColor]];
//    [textLabel setTextColor:[UIColor whiteColor]];
//    
//    // 设置文字控件的宽度按照上一级视图（topView）的比例进行缩放
//    [textLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//    // 设置View控件的宽度按照父视图的比例进行缩放，距离父视图顶部、左边距和右边距的距离不变
//    [topView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin];
//    topView.autoresizesSubviews = YES;
//    // 添加视图
//    [topView addSubview:textLabel];
//    [self.view addSubview:topView];
    
    // 注意：重新设置topView位置的代码，必须要写在添加视图的后面，不然autoresizing的位置计算会出错！
    
//    CGFloat topViewWidth = [UIScreen mainScreen].bounds.size.width - kMargin * 2;
//    [topView setFrame:CGRectMake(kMargin, topSpace, topViewWidth, kTopViewHeight)];
    UIView *v1 = [[UIView alloc] initWithFrame:CGRectMake(30, 30, self.view.bounds.size.width-30*2, 100)];
    v1.backgroundColor = [UIColor grayColor];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(20, 10, v1.bounds.size.width-40, 30)];
    btn.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    btn.backgroundColor = [UIColor redColor];
    v1.autoresizesSubviews = YES;
    [v1 addSubview:btn];
    self.btn = btn;
    
    
    UIButton *btn2 = [[UIButton alloc] init];
    CGRect btn2Frame = v1.frame;
    btn2Frame.origin.y = CGRectGetMaxY(btn2Frame)+20;
    btn2Frame.size.height = 50;
    btn2.frame = btn2Frame;
    btn2.backgroundColor = [UIColor redColor];
    [btn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn2 setTitle:@"click" forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(click1:) forControlEvents:UIControlEventTouchUpInside];
    btn2.tag = 1;
    [self.view addSubview:btn2];
    
    [self.view addSubview:v1];
    self.v1 = v1;
}


- (void)click1:(id)obj{
    NSLog(@"%@",NSStringFromCGRect(self.v1.frame));
    NSLog(@"%@",NSStringFromCGRect([_btn frame]));
    if([obj isKindOfClass:[UIButton class]]){
        UIButton *btn = (UIButton*)(obj);
        if(btn.tag==1){
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:.5];
            CGRect frame = self.v1.frame;
            if(frame.size.width>=20){
                frame.size.width-=20;
            }
            self.v1.frame = frame;
            [UIView commitAnimations];
        }
    }
    NSLog(@"----------------------------------");
    NSLog(@"%@",NSStringFromCGRect(self.v1.frame));
    NSLog(@"%@",NSStringFromCGRect([_btn frame]));
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
