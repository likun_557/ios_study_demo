//
//  AppDelegate.h
//  4.2、自动布局2
//
//  Created by 李坤 on 16/12/20.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

