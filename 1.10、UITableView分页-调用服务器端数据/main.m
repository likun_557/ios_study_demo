//
//  main.m
//  1.10、UITableView分页-调用服务器端数据
//
//  Created by 李坤 on 16/12/1.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
