//
//  YYappView.h
//  iOS
//
//  Created by 李坤 on 16/12/6.
//
//

#import <UIKit/UIKit.h>

@interface YYappView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *appImg;
@property (weak, nonatomic) IBOutlet UILabel *appName;
@property (weak, nonatomic) IBOutlet UIButton *appbtn;

@end
