//
//  LFViewController.m
//  iOS
//
//  Created by 李坤 on 16/12/6.
//
//

#import "LFViewController.h"
#import "HeroModel.h"
#import "YYappView.h"

@interface LFViewController ()

@property(nonatomic,strong)NSArray<HeroModel *> *appList;

@end

@implementation LFViewController

//使用懒加载获取数据
- (NSArray *)appList{
    if(!_appList) {
        //从mainbundle加载数据
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *path = [bundle pathForResource:@"app" ofType:@"plist"];
        NSLog(@"%@",path);
        NSArray *array = [NSArray arrayWithContentsOfFile:path];
        _appList = [HeroModel herosWithArrayOfDict:array];
        
    }
    return _appList;
}

#warning xxx

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //总共有3列
    int totalCol = 3;
    CGFloat viewW = 80;
    CGFloat viewH = 120;
    NSInteger marginX = (self.view.bounds.size.width - totalCol * viewW) / (totalCol+1);
    CGFloat marginY = 10;
    CGFloat startY = 20;
    for(int i = 0; i < self.appList.count; i++){
        
        int row = i / totalCol;
        int col = i % totalCol;
        CGFloat x = marginX + (viewW + marginX) * col;
        CGFloat y = startY + marginY + (viewH + marginY) * row;
        
        HeroModel *heroModel = self.appList[i];
        
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"appxib" owner:nil options:nil];
        NSLog(@"%@",array);
        YYappView *appview = array.firstObject;
        appview.frame = CGRectMake(x, y, viewW, viewH);
        [self.view addSubview:appview];
        
        appview.appImg.image = [UIImage imageNamed:heroModel.icon];
        
        appview.appName.text = heroModel.name;
        
        appview.appbtn.tag = i;
        [appview.appbtn addTarget:self action:@selector(appviewbtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
}

- (void)appviewbtnClick:(UIButton *)btn{
    HeroModel *hm = self.appList[btn.tag];
    UILabel *showlab=[[UILabel alloc]initWithFrame:CGRectMake(10, 450, 200, 40)];
    showlab.center = CGPointMake(self.view.center.x,450);
    showlab.textAlignment = NSTextAlignmentCenter;
    [showlab setText:[NSString stringWithFormat: @"%@下载成功",hm.name]];
    [showlab setBackgroundColor:[UIColor redColor]];
    [showlab setTextColor:[UIColor whiteColor]];
    showlab.layer.masksToBounds = YES;
    showlab.layer.cornerRadius = 5.0;
    showlab.layer.borderWidth = 2.0;
    showlab.layer.borderColor = [[UIColor whiteColor] CGColor];
    [self.view addSubview:showlab];
    showlab.alpha=1.0;
    [UIView animateWithDuration:2.0 animations:^{
        showlab.alpha = 0;
    }completion:^(BOOL finished) {
        [showlab removeFromSuperview];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
