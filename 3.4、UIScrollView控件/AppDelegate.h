//
//  AppDelegate.h
//  3.4、UIScrollView控件
//
//  Created by 李坤 on 16/12/6.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

