//
//  ViewController.m
//  3.4、UIScrollView控件
//
//  Created by 李坤 on 16/12/6.
//
//

#import "ViewController.h"

@interface ViewController ()<UIScrollViewDelegate>
{
    //在私有扩展中创建一个属性
    UIScrollView *_scrollView;
    UIImageView *_imageView;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //1、创建UIScrollView
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.frame = CGRectMake(0, 0, 350, 350);//frame中的size指UIScrollView的可视范围
    scrollView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:scrollView];
    
    //2、创建UIIMageView（图片）
    _imageView= [[UIImageView alloc] init];
    _imageView.backgroundColor = [UIColor whiteColor];
    _imageView.image = [UIImage imageNamed:@"1"];
    CGFloat imgW = _imageView.image.size.width;
    CGFloat imgH = _imageView.image.size.height;
    _imageView.frame = CGRectMake(0, 0, imgW, imgH);
    [scrollView addSubview:_imageView];
    
    //3、设置scrollView属性
    //设置UIScrollView的滚动范围（内容大小）
    scrollView.contentSize = _imageView.image.size;
    
    //隐藏股东条
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    
    //用来记录scrollview滚动的位置
    //scrollView.contentOffset = ;
    
    //增加额外的滚动区域（逆时针：上、左、下、右）
    scrollView.contentInset = UIEdgeInsetsMake(20, 20, 20, 20);
    
    _scrollView = scrollView;
    //设置代理scrollview的代理对象
    _scrollView.delegate = self;
    //设置最大伸缩比例
    _scrollView.maximumZoomScale = 2.0;
    //设置最小伸缩比例
    _scrollView.minimumZoomScale = 0.5;
//    _scrollView.scrollEnabled = NO;
    
}
/**
 *  uiscrollview滚动完毕之后触发
 *
 *  @param scrollView scrollView对象
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"scrollView.contentOffset = %@",NSStringFromCGPoint(scrollView.contentOffset));
}

//用户使用啮合手势的时候调用
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    return _imageView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
