//
//  ViewController.m
//  3.13、程序启动原理和UIApplication
//
//  Created by 李坤 on 16/12/9.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat padding = 30;
    //创建并添加一个按钮
    UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(padding, padding, self.view.bounds.size.width - padding*2, 30)];
    [btn1 setTitle:@"设置应用程序图标右上角的红色提醒数字" forState:UIControlStateNormal];
    [btn1 setBackgroundColor:[UIColor brownColor]];
    [btn1 addTarget:self action:@selector(onClick1) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn1];
    
    UIButton *btn2 = [[UIButton alloc] init];
    CGRect frame2 = btn1.frame;
    frame2.origin.y=CGRectGetMaxY(btn1.frame) + padding;
    btn2.frame = frame2;
    [btn2 setTitle:@"设置联网指示器课件" forState:UIControlStateNormal];
    [btn2 setBackgroundColor:[UIColor brownColor]];
    [btn2 addTarget:self action:@selector(onClick2) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn2];
    
    //通过Appction来管理应用程序的状态
    UIButton *btn3 = [[UIButton alloc] init];
    CGRect frame3 = btn2.frame;
    frame3.origin.y=CGRectGetMaxY(btn2.frame) + padding;
    btn3.frame = frame3;
    [btn3 setTitle:@"通过UIApplication来管理程序的各种状态" forState:UIControlStateNormal];
    [btn3 setBackgroundColor:[UIColor brownColor]];
    [btn3 addTarget:self action:@selector(onClick3) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn3];
    
    //打电话
    UIButton *btn4 = [[UIButton alloc] init];
    CGRect frame4 = btn3.frame;
    frame4.origin.y=CGRectGetMaxY(btn3.frame) + padding;
    btn4.frame = frame4;
    [btn4 setTitle:@"拨打10086" forState:UIControlStateNormal];
    [btn4 setBackgroundColor:[UIColor brownColor]];
    [btn4 addTarget:self action:@selector(onClick4) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn4];
    
    //发短信
    UIButton *btn5 = [[UIButton alloc] init];
    CGRect frame5 = btn4.frame;
    frame5.origin.y=CGRectGetMaxY(btn4.frame) + padding;
    btn5.frame = frame5;
    [btn5 setTitle:@"给10086发送短信" forState:UIControlStateNormal];
    [btn5 setBackgroundColor:[UIColor brownColor]];
    [btn5 addTarget:self action:@selector(onClick5) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn5];
    
    //打开网址
    UIButton *btn6 = [[UIButton alloc] init];
    CGRect frame6 = btn5.frame;
    frame6.origin.y=CGRectGetMaxY(btn5.frame) + padding;
    btn6.frame = frame6;
    [btn6 setTitle:@"打开http://www.itsoku.com" forState:UIControlStateNormal];
    [btn6 setBackgroundColor:[UIColor brownColor]];
    [btn6 addTarget:self action:@selector(onClick6) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn6];
}

#pragma with - 打开网址
- (void)onClick6{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.itsoku.com"]];
}

#pragma mark 发短信
- (void)onClick5{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"sms:10086"]];
}

#pragma mark 打电话
- (void)onClick4{
    UIApplication *app = [UIApplication sharedApplication];
    [app openURL:[NSURL URLWithString:@"tel://10086"]];
}

- (void)onClick3{
    //通过shareApplication获取程序的UIApplication对象
    UIApplication *app = [UIApplication sharedApplication];
    app.applicationIconBadgeNumber = 123;
    
    //设置指示器的联网动画
    app.networkActivityIndicatorVisible = YES;
    //设置状态栏的样式
    app.statusBarStyle = UIStatusBarStyleDefault;//默认黑色
    //设置白色+动画
    [app setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
}

#pragma with - 设置状态栏的样式
- (UIStatusBarStyle)preferredStatusBarStyle {
    //设置为白色
//    return UIStatusBarStyleLightContent;
    //默认为黑色
    return UIStatusBarStyleDefault;
}

#pragma with - 设置状态栏是否隐藏（否）
- (BOOL)prefersStatusBarHidden{
    return NO;
}

- (void)onClick2{
    UIApplication *app = [UIApplication sharedApplication];
    //设置指示器的联网动画
    app.networkActivityIndicatorVisible = YES;
}

- (void)onClick1{
    NSLog(@"%s,按钮点击事件",__func__);
    //错误，只能有一个唯一的UIApplication对象，不能再进行创建
//    UIApplication *app = [[UIApplication alloc] init];
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if (version >= 8.0) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    //通过shareApplication获取该程序的UIApplication对象
    UIApplication *app = [UIApplication sharedApplication];
    app.applicationIconBadgeNumber = 123;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
