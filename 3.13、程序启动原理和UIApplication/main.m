//
//  main.m
//  3.13、程序启动原理和UIApplication
//
//  Created by 李坤 on 16/12/9.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        NSLog(@"%i",argc);
        for (int i=0; i<argc; i++) {
            NSLog(@"%s",argv[i]);
        }
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
