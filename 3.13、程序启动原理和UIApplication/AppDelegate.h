//
//  AppDelegate.h
//  3.13、程序启动原理和UIApplication
//
//  Created by 李坤 on 16/12/9.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

